package gogen

import (
	"reflect"
)

func evalsTrue(val interface{}) bool {
	if val == nil {
		return false
	}
	rv := reflect.ValueOf(val)

	if rv.Kind() == reflect.Slice {
		// the empty slice is considered false
		return rv.Len() > 0
	}

	return !rv.IsZero()
}
