package parser

import (
	"errors"

	"github.com/antlr4-go/antlr/v4"
	"gitlab.com/akabio/gogen/common"
)

func (v *TemplateVisitor) SyntaxError(_ antlr.Recognizer, _ interface{},
	line, column int, msg string, _ antlr.RecognitionException,
) {
	antLoc := common.Loc("", line-1, column) // line is 1 base, column is zero based
	v.errors.PushLocation(v.loc.Add(antLoc), errors.New(msg))
}

func (v *TemplateVisitor) ReportAmbiguity(_ antlr.Parser, _ *antlr.DFA, _,
	_ int, _ bool, _ *antlr.BitSet, _ *antlr.ATNConfigSet,
) {
	// fmt.Println("ReportAmbiguity")
}

func (v *TemplateVisitor) ReportAttemptingFullContext(_ antlr.Parser, _ *antlr.DFA,
	_, _ int, _ *antlr.BitSet, _ *antlr.ATNConfigSet,
) {
	// fmt.Println("ReportAttemptingFullContext")
}

func (v *TemplateVisitor) ReportContextSensitivity(_ antlr.Parser, _ *antlr.DFA,
	_, _, _ int, _ *antlr.ATNConfigSet,
) {
	// fmt.Println("ReportContextSensitivity")
}
