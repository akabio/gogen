package parser

import (
	"fmt"
	"strconv"

	"github.com/antlr4-go/antlr/v4"
	"github.com/pkg/errors"
	"gitlab.com/akabio/gogen/ast"
	"gitlab.com/akabio/gogen/common"
	"gitlab.com/akabio/gogen/ggerr"
	"gitlab.com/akabio/gogen/internal/parser/ggt"
)

func createParserFor(i string, l common.Location) (*ggt.GoGenTemplateParser, *TemplateVisitor) {
	input := antlr.NewInputStream(i)
	lexer := ggt.NewGoGenTemplateLexer(input)
	stream := antlr.NewCommonTokenStream(lexer, 0)
	p := ggt.NewGoGenTemplateParser(stream)
	p.BuildParseTrees = true

	visi := &TemplateVisitor{loc: l, errors: ggerr.NewMultiError()}

	p.RemoveErrorListeners()
	p.AddErrorListener(visi)

	return p, visi
}

func parseVisitDefaultParameters(i string, l common.Location) ([]*ast.Parameter, *ggerr.MultiError) {
	parser, visitor := createParserFor(i, l)
	result := parser.VisitDefaultParameters().Accept(visitor)

	return result.([]*ast.Parameter), visitor.errors
}

func parseImportParameters(i string, l common.Location) ([]*ast.Parameter, *ggerr.MultiError) {
	parser, visitor := createParserFor(i, l)
	result := parser.ImportParameters().Accept(visitor)

	return result.([]*ast.Parameter), visitor.errors
}

func parseVisit(i string, l common.Location) (*ast.VisitNode, *ggerr.MultiError) {
	parser, visitor := createParserFor(i, l)
	result := parser.View().Accept(visitor)

	return result.(*ast.VisitNode), visitor.errors
}

func parseEcho(i string, l common.Location) (*ast.EchoNode, *ggerr.MultiError) {
	parser, visitor := createParserFor(i, l)
	result := parser.Echo().Accept(visitor)

	return result.(*ast.EchoNode), visitor.errors
}

func parseFor(i string, l common.Location) (*ast.ForNode, *ggerr.MultiError) {
	parser, visitor := createParserFor(i, l)
	result := parser.ForBlock().Accept(visitor)

	return result.(*ast.ForNode), visitor.errors
}

func parseIndent(i string, l common.Location) (*ast.IndentNode, *ggerr.MultiError) {
	parser, visitor := createParserFor(i, l)
	result := parser.IndentBlock().Accept(visitor)

	return result.(*ast.IndentNode), visitor.errors
}

func parseIf(i string, l common.Location) (*ast.IfNode, *ggerr.MultiError) {
	parser, visitor := createParserFor(i, l)
	result := parser.IfBlock().Accept(visitor)

	return result.(*ast.IfNode), visitor.errors
}

func parseElse(i string, l common.Location) (ast.Silent, *ggerr.MultiError) {
	parser, visitor := createParserFor(i, l)
	result := parser.ElseBlock().Accept(visitor)

	return result.(ast.Silent), visitor.errors
}

func parseEnd(i string, l common.Location) (ast.Silent, *ggerr.MultiError) {
	parser, visitor := createParserFor(i, l)
	result := parser.End().Accept(visitor)

	return result.(ast.Silent), visitor.errors
}

type TemplateVisitor struct {
	*ggt.BaseGoGenTemplateVisitor
	loc    common.Location
	errors *ggerr.MultiError
}

func (v *TemplateVisitor) VisitEcho(ctx *ggt.EchoContext) interface{} {
	return &ast.EchoNode{
		BaseNode:   v.createBaseNodeFromRule(ctx),
		Expression: ctx.Expression().Accept(v).(ast.Expression),
		Silent:     parseSilent(ctx.Opn(), ctx.Cls()),
	}
}

func (v *TemplateVisitor) VisitView(ctx *ggt.ViewContext) interface{} {
	vn := &ast.VisitNode{
		BaseNode: v.createBaseNodeFromRule(ctx),
		Silent:   parseSilent(ctx.Opn(), ctx.Cls()),
	}
	if ctx.Expression() != nil {
		vn.Expression = ctx.Expression().Accept(v).(ast.Expression)
	}

	if ctx.GetPkg() != nil {
		vn.Package = ctx.GetPkg().GetText()
	}

	if ctx.GetName() != nil {
		vn.Visitor = ctx.GetName().GetText()
	}

	for _, param := range ctx.AllViewParam() {
		vn.Parameters = append(vn.Parameters, param.Accept(v).(*ast.Parameter))
	}

	return vn
}

func (v *TemplateVisitor) VisitViewParam(ctx *ggt.ViewParamContext) interface{} {
	param := &ast.Parameter{
		BaseNode: v.createBaseNodeFromRule(ctx),
		Name:     ctx.IDENTIFIER().GetText(),
	}

	expr := ctx.Expression()
	if expr != nil {
		param.Value = expr.Accept(v).(ast.Expression)
	}

	return param
}

func (v *TemplateVisitor) VisitVisitDefaultParameters(ctx *ggt.VisitDefaultParametersContext) interface{} {
	params := []*ast.Parameter{}

	for _, c := range ctx.AllDefaultParam() {
		params = append(params, c.Accept(v).(*ast.Parameter))
	}

	return params
}

func (v *TemplateVisitor) VisitDefaultParam(ctx *ggt.DefaultParamContext) interface{} {
	param := &ast.Parameter{
		BaseNode: v.createBaseNodeFromRule(ctx),
		Name:     ctx.IDENTIFIER().GetText(),
	}

	expr := ctx.Literal()
	if expr != nil {
		param.Value = expr.Accept(v).(ast.Expression)
	}

	return param
}

func (v *TemplateVisitor) VisitImportParameters(ctx *ggt.ImportParametersContext) interface{} {
	params := []*ast.Parameter{}

	for _, c := range ctx.AllImportParameter() {
		params = append(params, c.Accept(v).(*ast.Parameter))
	}

	return params
}

func (v *TemplateVisitor) VisitImportParameter(ctx *ggt.ImportParameterContext) interface{} {
	param := &ast.Parameter{
		BaseNode: v.createBaseNodeFromRule(ctx),
		Name:     ctx.IDENTIFIER().GetText(),
	}

	expr := ctx.Expression()
	if expr != nil {
		param.Value = expr.Accept(v).(ast.Expression)
	}

	return param
}

func (v *TemplateVisitor) VisitForBlock(ctx *ggt.ForBlockContext) interface{} {
	loop := &ast.ForNode{
		BaseNode:  v.createBaseNodeFromRule(ctx),
		First:     len(ctx.AllFIRST()) > 0,
		Last:      len(ctx.AllLAST()) > 0,
		Even:      len(ctx.AllEVEN()) > 0,
		SilentFor: parseSilent(ctx.Opn(), ctx.Cls()),
	}

	if ctx.GetValue() == nil {
		loop.Value = ctx.GetKey().GetText()
	} else {
		loop.Key = ctx.GetKey().GetText()
		loop.Value = ctx.GetValue().GetText()
	}

	if ctx.Expression() != nil {
		loop.Expression = ctx.Expression().Accept(v).(ast.Expression)
	}

	return loop
}

func (v *TemplateVisitor) VisitIndentBlock(ctx *ggt.IndentBlockContext) interface{} {
	indent := &ast.IndentNode{
		BaseNode:     v.createBaseNodeFromRule(ctx),
		SilentIndent: parseSilent(ctx.Opn(), ctx.Cls()),
	}
	if ctx.Expression() != nil {
		indent.Expression = ctx.Expression().Accept(v).(ast.Expression)
	}

	return indent
}

func (v *TemplateVisitor) VisitIfBlock(ctx *ggt.IfBlockContext) interface{} {
	ifelse := &ast.IfNode{
		BaseNode:   v.createBaseNodeFromRule(ctx),
		Expression: ctx.Expression().Accept(v).(ast.Expression),
		SilentIf:   parseSilent(ctx.Opn(), ctx.Cls()),
	}

	return ifelse
}

func (v *TemplateVisitor) VisitElseBlock(ctx *ggt.ElseBlockContext) interface{} {
	return parseSilent(ctx.Opn(), ctx.Cls())
}

func (v *TemplateVisitor) VisitEnd(ctx *ggt.EndContext) interface{} {
	return parseSilent(ctx.Opn(), ctx.Cls())
}

func (v *TemplateVisitor) VisitRootExpression(ctx *ggt.RootExpressionContext) interface{} {
	return ctx.Expression().Accept(v)
}

func (v *TemplateVisitor) VisitExpression(ctx *ggt.ExpressionContext) interface{} {
	if ctx.Operand() != nil {
		return ctx.Operand().Accept(v)
	}

	if ctx.UnaryExpression() != nil {
		return ctx.UnaryExpression().Accept(v)
	}

	all := ctx.AllExpression()
	if len(all) == 2 {
		return &ast.BinaryOperation{
			BaseNode: v.createBaseNodeFromRule(ctx),
			Left:     all[0].Accept(v).(ast.Expression),
			Right:    all[1].Accept(v).(ast.Expression),
			Operator: ctx.GetOperator().GetText(),
		}
	}

	if ctx.GetFilter() != nil {
		return &ast.Filter{
			BaseNode:   v.createBaseNode(ctx.GetFilter()),
			Name:       ctx.GetFilter().GetText(),
			Expression: all[0].Accept(v).(ast.Expression),
		}
	}

	// statements like 'x+' will be parsed as good as possible which can lead to
	// empty epxressions in result. An error will be added already.
	return &ast.NoExpression{}
}

func (v *TemplateVisitor) VisitUnaryExpression(ctx *ggt.UnaryExpressionContext) interface{} {
	if ctx.NOT() != nil {
		return &ast.NotOperation{
			BaseNode:   v.createBaseNodeFromRule(ctx),
			Expression: ctx.Expression().Accept(v).(ast.Expression),
		}
	}

	if ctx.EXISTS() != nil {
		return &ast.VarExists{
			BaseNode: v.createBaseNodeFromRule(ctx),
			Name:     ctx.Variable().GetText(),
		}
	}

	panic(fmt.Errorf("unexpected unary expression %v", ctx.GetText()))
}

func (v *TemplateVisitor) VisitOperand(ctx *ggt.OperandContext) interface{} {
	if ctx.Literal() != nil {
		return ctx.Literal().Accept(v)
	}

	if ctx.SelectorChain() != nil {
		return ctx.SelectorChain().Accept(v)
	}

	if ctx.Expression() != nil {
		return ctx.Expression().Accept(v)
	}

	panic("invalid operand")
}

func (v *TemplateVisitor) VisitLiteral(ctx *ggt.LiteralContext) interface{} {
	if ctx.BoolLiteral() != nil {
		return ctx.BoolLiteral().Accept(v)
	}

	if ctx.IntLiteral() != nil {
		return ctx.IntLiteral().Accept(v)
	}

	if ctx.FloatLiteral() != nil {
		return ctx.FloatLiteral().Accept(v)
	}

	if ctx.StringLiteral() != nil {
		return ctx.StringLiteral().Accept(v)
	}

	panic("unknown literal")
}

func (v *TemplateVisitor) VisitBoolLiteral(ctx *ggt.BoolLiteralContext) interface{} {
	return &ast.BoolLiteral{BaseNode: v.createBaseNodeFromRule(ctx), Value: ctx.GetText() == "true"}
}

func (v *TemplateVisitor) VisitIntLiteral(ctx *ggt.IntLiteralContext) interface{} {
	i, err := strconv.ParseInt(ctx.GetText(), 10, 64)
	if err != nil {
		panic(errors.WithStack(err))
	}

	return &ast.IntLiteral{BaseNode: v.createBaseNodeFromRule(ctx), Value: i}
}

func (v *TemplateVisitor) VisitFloatLiteral(ctx *ggt.FloatLiteralContext) interface{} {
	i, err := strconv.ParseFloat(ctx.GetText(), 64)
	if err != nil {
		panic(errors.WithStack(err))
	}

	return &ast.FloatLiteral{BaseNode: v.createBaseNodeFromRule(ctx), Value: i}
}

func (v *TemplateVisitor) VisitStringLiteral(ctx *ggt.StringLiteralContext) interface{} {
	str := ctx.GetText()
	str = str[1 : len(str)-1]

	return &ast.StringLiteral{BaseNode: v.createBaseNodeFromRule(ctx), Value: str}
}

func (v *TemplateVisitor) VisitSelectorChain(ctx *ggt.SelectorChainContext) interface{} {
	sc := &ast.SelectorChain{
		BaseNode: v.createBaseNodeFromRule(ctx),
	}

	if ctx.Variable() != nil {
		sc.Selectors = append(sc.Selectors, ctx.Variable().Accept(v).(*ast.VariableSelector))
	}

	for _, s := range ctx.AllSelector() {
		sc.Selectors = append(sc.Selectors, s.Accept(v).(ast.Selector))
	}

	if ctx.RootSelector() != nil {
		sc.Selectors = append(sc.Selectors, ctx.RootSelector().Accept(v).(ast.Selector))
	}

	return sc
}

func (v *TemplateVisitor) VisitRootSelector(ctx *ggt.RootSelectorContext) interface{} {
	sc := &ast.SelectorChain{
		BaseNode: v.createBaseNodeFromRule(ctx),
	}

	if ctx.Index() != nil {
		sc.Selectors = append(sc.Selectors, ctx.Index().Accept(v).(ast.Selector))

		if ctx.SelectorChain() != nil {
			sc.Selectors = append(sc.Selectors, ctx.SelectorChain().Accept(v).(*ast.SelectorChain))
		}
	}

	return sc
}

func (v *TemplateVisitor) VisitSelector(ctx *ggt.SelectorContext) interface{} {
	if ctx.FieldSelector() != nil {
		return ctx.FieldSelector().Accept(v)
	}

	if ctx.Call() != nil {
		return ctx.Call().Accept(v)
	}

	if ctx.Index() != nil {
		return ctx.Index().Accept(v)
	}

	if ctx.Slice() != nil {
		return ctx.Slice().Accept(v)
	}

	panic("invalid selector")
}

func (v *TemplateVisitor) VisitIndex(ctx *ggt.IndexContext) interface{} {
	return &ast.IndexSelector{
		BaseNode:   v.createBaseNodeFromRule(ctx),
		Expression: ctx.Expression().Accept(v).(ast.Expression),
	}
}

func (v *TemplateVisitor) VisitSlice(ctx *ggt.SliceContext) interface{} {
	var from ast.Expression
	if ctx.GetFrom() != nil {
		from = ctx.GetFrom().Accept(v).(ast.Expression)
	}

	var to ast.Expression
	if ctx.GetTo() != nil {
		to = ctx.GetTo().Accept(v).(ast.Expression)
	}

	return &ast.SliceSelector{
		BaseNode: v.createBaseNodeFromRule(ctx),
		From:     from,
		To:       to,
	}
}

func (v *TemplateVisitor) VisitVariable(ctx *ggt.VariableContext) interface{} {
	return &ast.VariableSelector{
		BaseNode: v.createBaseNodeFromRule(ctx),
		Name:     ctx.Identifier().GetText(),
	}
}

func (v *TemplateVisitor) VisitFieldSelector(ctx *ggt.FieldSelectorContext) interface{} {
	return &ast.FieldSelector{
		BaseNode: v.createBaseNodeFromRule(ctx),
		Name:     ctx.GetText()[1:],
	}
}

func (v *TemplateVisitor) VisitCall(ctx *ggt.CallContext) interface{} {
	return &ast.CallSelector{
		BaseNode: v.createBaseNodeFromRule(ctx),
	}
}
