package parser

import (
	"testing"

	"gitlab.com/akabio/gogen/common"
)

func BenchmarkParseTemplate(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, err := parseControl(benchmarkTemplate, common.Loc("bench.gg", 1, 1))
		if err.Has() {
			b.Fatal(err)
		}
	}
}

var benchmarkTemplate = `
Hello bla bla
{for foo in .Foos}
	{if true}
		We are true or also !false or somehow weird
	{end}

{ end }
Hello bla bla
Hello bla bla
{for foo in .Foos}
	{if true}
		We are true or also !false or somehow weird
	{end}
	{for foo in .Foos}
		{if true}
			We are true or also !false or somehow weird
		{end}
	{ end }
{ end }
Hello bla bla

`
