grammar GoGenTemplate;

FOR: 'for';
IN: 'in';
END: 'end';
IF: 'if';
INDENT: 'indent';
ELSE: 'else';
FIRST: 'first';
LAST: 'last';
EVEN: 'even';

STRING: '"' .*? '"';
IDENTIFIER: [A-Za-z][a-zA-Z0-9]*;
INT: [0-9]+;
DOT: '.';
NOT: '!';
EXISTS: '?';

WS: [ \t]+ -> skip;

identifier:
	IDENTIFIER
	| FOR
	| IN
	| END
	| IF
	| ELSE
	| FIRST
	| LAST
	| EVEN
	| INDENT;

opn: '{-' | '{';
cls: '-}' | '}';

end: opn END cls;

forBlock:
	opn FOR key = identifier (',' value = identifier)? (
		',' (FIRST | LAST | EVEN)
	)* IN expression cls;

ifBlock: opn IF expression cls;
elseBlock: opn ELSE cls;

indentBlock: opn INDENT expression? cls;

echo: '$' opn expression cls;

view:
	'@' opn ((pkg = IDENTIFIER '.')? name = IDENTIFIER ':')? expression (
		viewParam (',' viewParam)*
	)? cls;

viewParam: IDENTIFIER '=' expression;


visitDefaultParameters: (defaultParam (',' defaultParam)*)?;

defaultParam: IDENTIFIER '=' literal;

importParameters: (importParameter (',' importParameter)*)?;

importParameter: IDENTIFIER '=' expression;

rootExpression: expression EOF;

expression:
	operand
	| unaryExpression
	| expression operator = ('*' | '/' | '%') expression
	| expression operator = ('+' | '-') expression
	| expression operator = (
		'=='
		| '!='
		| '<'
		| '<='
		| '>'
		| '>='
	) expression
	| expression operator = '&&' expression
	| expression operator = '||' expression
	| expression '|' filter = IDENTIFIER;

operand: literal | selectorChain | '(' expression ')';

unaryExpression: NOT expression | EXISTS variable ;

literal:
	boolLiteral
	| intLiteral
	| floatLiteral
	| stringLiteral;

boolLiteral: 'true' | 'false';
intLiteral: '-'? INT;
floatLiteral: '-'? INT '.' INT;
stringLiteral: STRING;

// selectorChain is the most top selector element
selectorChain: (
		(variable | variable selector+ | selector+)
		| rootSelector
	);

// root selector to not allow ..Field but . or .[3].Field
rootSelector: DOT (index selectorChain?)?;

selector: fieldSelector | index | slice | call;

fieldSelector: DOT identifier;

index: '[' expression ']';

slice: '[' from=expression? ':' to=expression? ']';

call: '(' ')';

variable: identifier;