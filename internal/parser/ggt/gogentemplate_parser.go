// Code generated from ./GoGenTemplate.g4 by ANTLR 4.13.0. DO NOT EDIT.

package ggt // GoGenTemplate
import (
	"fmt"
	"strconv"
	"sync"

	"github.com/antlr4-go/antlr/v4"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = strconv.Itoa
var _ = sync.Once{}

type GoGenTemplateParser struct {
	*antlr.BaseParser
}

var GoGenTemplateParserStaticData struct {
	once                   sync.Once
	serializedATN          []int32
	LiteralNames           []string
	SymbolicNames          []string
	RuleNames              []string
	PredictionContextCache *antlr.PredictionContextCache
	atn                    *antlr.ATN
	decisionToDFA          []*antlr.DFA
}

func gogentemplateParserInit() {
	staticData := &GoGenTemplateParserStaticData
	staticData.LiteralNames = []string{
		"", "'{-'", "'{'", "'-}'", "'}'", "','", "'$'", "'@'", "':'", "'='",
		"'*'", "'/'", "'%'", "'+'", "'-'", "'=='", "'!='", "'<'", "'<='", "'>'",
		"'>='", "'&&'", "'||'", "'|'", "'('", "')'", "'true'", "'false'", "'['",
		"']'", "'for'", "'in'", "'end'", "'if'", "'indent'", "'else'", "'first'",
		"'last'", "'even'", "", "", "", "'.'", "'!'", "'?'",
	}
	staticData.SymbolicNames = []string{
		"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
		"", "", "", "", "", "", "", "", "", "", "", "", "", "FOR", "IN", "END",
		"IF", "INDENT", "ELSE", "FIRST", "LAST", "EVEN", "STRING", "IDENTIFIER",
		"INT", "DOT", "NOT", "EXISTS", "WS",
	}
	staticData.RuleNames = []string{
		"identifier", "opn", "cls", "end", "forBlock", "ifBlock", "elseBlock",
		"indentBlock", "echo", "view", "viewParam", "visitDefaultParameters",
		"defaultParam", "importParameters", "importParameter", "rootExpression",
		"expression", "operand", "unaryExpression", "literal", "boolLiteral",
		"intLiteral", "floatLiteral", "stringLiteral", "selectorChain", "rootSelector",
		"selector", "fieldSelector", "index", "slice", "call", "variable",
	}
	staticData.PredictionContextCache = antlr.NewPredictionContextCache()
	staticData.serializedATN = []int32{
		4, 1, 45, 288, 2, 0, 7, 0, 2, 1, 7, 1, 2, 2, 7, 2, 2, 3, 7, 3, 2, 4, 7,
		4, 2, 5, 7, 5, 2, 6, 7, 6, 2, 7, 7, 7, 2, 8, 7, 8, 2, 9, 7, 9, 2, 10, 7,
		10, 2, 11, 7, 11, 2, 12, 7, 12, 2, 13, 7, 13, 2, 14, 7, 14, 2, 15, 7, 15,
		2, 16, 7, 16, 2, 17, 7, 17, 2, 18, 7, 18, 2, 19, 7, 19, 2, 20, 7, 20, 2,
		21, 7, 21, 2, 22, 7, 22, 2, 23, 7, 23, 2, 24, 7, 24, 2, 25, 7, 25, 2, 26,
		7, 26, 2, 27, 7, 27, 2, 28, 7, 28, 2, 29, 7, 29, 2, 30, 7, 30, 2, 31, 7,
		31, 1, 0, 1, 0, 1, 1, 1, 1, 1, 2, 1, 2, 1, 3, 1, 3, 1, 3, 1, 3, 1, 4, 1,
		4, 1, 4, 1, 4, 1, 4, 3, 4, 80, 8, 4, 1, 4, 1, 4, 5, 4, 84, 8, 4, 10, 4,
		12, 4, 87, 9, 4, 1, 4, 1, 4, 1, 4, 1, 4, 1, 5, 1, 5, 1, 5, 1, 5, 1, 5,
		1, 6, 1, 6, 1, 6, 1, 6, 1, 7, 1, 7, 1, 7, 3, 7, 105, 8, 7, 1, 7, 1, 7,
		1, 8, 1, 8, 1, 8, 1, 8, 1, 8, 1, 9, 1, 9, 1, 9, 1, 9, 3, 9, 118, 8, 9,
		1, 9, 1, 9, 3, 9, 122, 8, 9, 1, 9, 1, 9, 1, 9, 1, 9, 5, 9, 128, 8, 9, 10,
		9, 12, 9, 131, 9, 9, 3, 9, 133, 8, 9, 1, 9, 1, 9, 1, 10, 1, 10, 1, 10,
		1, 10, 1, 11, 1, 11, 1, 11, 5, 11, 144, 8, 11, 10, 11, 12, 11, 147, 9,
		11, 3, 11, 149, 8, 11, 1, 12, 1, 12, 1, 12, 1, 12, 1, 13, 1, 13, 1, 13,
		5, 13, 158, 8, 13, 10, 13, 12, 13, 161, 9, 13, 3, 13, 163, 8, 13, 1, 14,
		1, 14, 1, 14, 1, 14, 1, 15, 1, 15, 1, 15, 1, 16, 1, 16, 1, 16, 3, 16, 175,
		8, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1,
		16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 1, 16, 5, 16, 195,
		8, 16, 10, 16, 12, 16, 198, 9, 16, 1, 17, 1, 17, 1, 17, 1, 17, 1, 17, 1,
		17, 3, 17, 206, 8, 17, 1, 18, 1, 18, 1, 18, 1, 18, 3, 18, 212, 8, 18, 1,
		19, 1, 19, 1, 19, 1, 19, 3, 19, 218, 8, 19, 1, 20, 1, 20, 1, 21, 3, 21,
		223, 8, 21, 1, 21, 1, 21, 1, 22, 3, 22, 228, 8, 22, 1, 22, 1, 22, 1, 22,
		1, 22, 1, 23, 1, 23, 1, 24, 1, 24, 1, 24, 4, 24, 239, 8, 24, 11, 24, 12,
		24, 240, 1, 24, 4, 24, 244, 8, 24, 11, 24, 12, 24, 245, 3, 24, 248, 8,
		24, 1, 24, 3, 24, 251, 8, 24, 1, 25, 1, 25, 1, 25, 3, 25, 256, 8, 25, 3,
		25, 258, 8, 25, 1, 26, 1, 26, 1, 26, 1, 26, 3, 26, 264, 8, 26, 1, 27, 1,
		27, 1, 27, 1, 28, 1, 28, 1, 28, 1, 28, 1, 29, 1, 29, 3, 29, 275, 8, 29,
		1, 29, 1, 29, 3, 29, 279, 8, 29, 1, 29, 1, 29, 1, 30, 1, 30, 1, 30, 1,
		31, 1, 31, 1, 31, 0, 1, 32, 32, 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20,
		22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56,
		58, 60, 62, 0, 8, 2, 0, 30, 38, 40, 40, 1, 0, 1, 2, 1, 0, 3, 4, 1, 0, 36,
		38, 1, 0, 10, 12, 1, 0, 13, 14, 1, 0, 15, 20, 1, 0, 26, 27, 293, 0, 64,
		1, 0, 0, 0, 2, 66, 1, 0, 0, 0, 4, 68, 1, 0, 0, 0, 6, 70, 1, 0, 0, 0, 8,
		74, 1, 0, 0, 0, 10, 92, 1, 0, 0, 0, 12, 97, 1, 0, 0, 0, 14, 101, 1, 0,
		0, 0, 16, 108, 1, 0, 0, 0, 18, 113, 1, 0, 0, 0, 20, 136, 1, 0, 0, 0, 22,
		148, 1, 0, 0, 0, 24, 150, 1, 0, 0, 0, 26, 162, 1, 0, 0, 0, 28, 164, 1,
		0, 0, 0, 30, 168, 1, 0, 0, 0, 32, 174, 1, 0, 0, 0, 34, 205, 1, 0, 0, 0,
		36, 211, 1, 0, 0, 0, 38, 217, 1, 0, 0, 0, 40, 219, 1, 0, 0, 0, 42, 222,
		1, 0, 0, 0, 44, 227, 1, 0, 0, 0, 46, 233, 1, 0, 0, 0, 48, 250, 1, 0, 0,
		0, 50, 252, 1, 0, 0, 0, 52, 263, 1, 0, 0, 0, 54, 265, 1, 0, 0, 0, 56, 268,
		1, 0, 0, 0, 58, 272, 1, 0, 0, 0, 60, 282, 1, 0, 0, 0, 62, 285, 1, 0, 0,
		0, 64, 65, 7, 0, 0, 0, 65, 1, 1, 0, 0, 0, 66, 67, 7, 1, 0, 0, 67, 3, 1,
		0, 0, 0, 68, 69, 7, 2, 0, 0, 69, 5, 1, 0, 0, 0, 70, 71, 3, 2, 1, 0, 71,
		72, 5, 32, 0, 0, 72, 73, 3, 4, 2, 0, 73, 7, 1, 0, 0, 0, 74, 75, 3, 2, 1,
		0, 75, 76, 5, 30, 0, 0, 76, 79, 3, 0, 0, 0, 77, 78, 5, 5, 0, 0, 78, 80,
		3, 0, 0, 0, 79, 77, 1, 0, 0, 0, 79, 80, 1, 0, 0, 0, 80, 85, 1, 0, 0, 0,
		81, 82, 5, 5, 0, 0, 82, 84, 7, 3, 0, 0, 83, 81, 1, 0, 0, 0, 84, 87, 1,
		0, 0, 0, 85, 83, 1, 0, 0, 0, 85, 86, 1, 0, 0, 0, 86, 88, 1, 0, 0, 0, 87,
		85, 1, 0, 0, 0, 88, 89, 5, 31, 0, 0, 89, 90, 3, 32, 16, 0, 90, 91, 3, 4,
		2, 0, 91, 9, 1, 0, 0, 0, 92, 93, 3, 2, 1, 0, 93, 94, 5, 33, 0, 0, 94, 95,
		3, 32, 16, 0, 95, 96, 3, 4, 2, 0, 96, 11, 1, 0, 0, 0, 97, 98, 3, 2, 1,
		0, 98, 99, 5, 35, 0, 0, 99, 100, 3, 4, 2, 0, 100, 13, 1, 0, 0, 0, 101,
		102, 3, 2, 1, 0, 102, 104, 5, 34, 0, 0, 103, 105, 3, 32, 16, 0, 104, 103,
		1, 0, 0, 0, 104, 105, 1, 0, 0, 0, 105, 106, 1, 0, 0, 0, 106, 107, 3, 4,
		2, 0, 107, 15, 1, 0, 0, 0, 108, 109, 5, 6, 0, 0, 109, 110, 3, 2, 1, 0,
		110, 111, 3, 32, 16, 0, 111, 112, 3, 4, 2, 0, 112, 17, 1, 0, 0, 0, 113,
		114, 5, 7, 0, 0, 114, 121, 3, 2, 1, 0, 115, 116, 5, 40, 0, 0, 116, 118,
		5, 42, 0, 0, 117, 115, 1, 0, 0, 0, 117, 118, 1, 0, 0, 0, 118, 119, 1, 0,
		0, 0, 119, 120, 5, 40, 0, 0, 120, 122, 5, 8, 0, 0, 121, 117, 1, 0, 0, 0,
		121, 122, 1, 0, 0, 0, 122, 123, 1, 0, 0, 0, 123, 132, 3, 32, 16, 0, 124,
		129, 3, 20, 10, 0, 125, 126, 5, 5, 0, 0, 126, 128, 3, 20, 10, 0, 127, 125,
		1, 0, 0, 0, 128, 131, 1, 0, 0, 0, 129, 127, 1, 0, 0, 0, 129, 130, 1, 0,
		0, 0, 130, 133, 1, 0, 0, 0, 131, 129, 1, 0, 0, 0, 132, 124, 1, 0, 0, 0,
		132, 133, 1, 0, 0, 0, 133, 134, 1, 0, 0, 0, 134, 135, 3, 4, 2, 0, 135,
		19, 1, 0, 0, 0, 136, 137, 5, 40, 0, 0, 137, 138, 5, 9, 0, 0, 138, 139,
		3, 32, 16, 0, 139, 21, 1, 0, 0, 0, 140, 145, 3, 24, 12, 0, 141, 142, 5,
		5, 0, 0, 142, 144, 3, 24, 12, 0, 143, 141, 1, 0, 0, 0, 144, 147, 1, 0,
		0, 0, 145, 143, 1, 0, 0, 0, 145, 146, 1, 0, 0, 0, 146, 149, 1, 0, 0, 0,
		147, 145, 1, 0, 0, 0, 148, 140, 1, 0, 0, 0, 148, 149, 1, 0, 0, 0, 149,
		23, 1, 0, 0, 0, 150, 151, 5, 40, 0, 0, 151, 152, 5, 9, 0, 0, 152, 153,
		3, 38, 19, 0, 153, 25, 1, 0, 0, 0, 154, 159, 3, 28, 14, 0, 155, 156, 5,
		5, 0, 0, 156, 158, 3, 28, 14, 0, 157, 155, 1, 0, 0, 0, 158, 161, 1, 0,
		0, 0, 159, 157, 1, 0, 0, 0, 159, 160, 1, 0, 0, 0, 160, 163, 1, 0, 0, 0,
		161, 159, 1, 0, 0, 0, 162, 154, 1, 0, 0, 0, 162, 163, 1, 0, 0, 0, 163,
		27, 1, 0, 0, 0, 164, 165, 5, 40, 0, 0, 165, 166, 5, 9, 0, 0, 166, 167,
		3, 32, 16, 0, 167, 29, 1, 0, 0, 0, 168, 169, 3, 32, 16, 0, 169, 170, 5,
		0, 0, 1, 170, 31, 1, 0, 0, 0, 171, 172, 6, 16, -1, 0, 172, 175, 3, 34,
		17, 0, 173, 175, 3, 36, 18, 0, 174, 171, 1, 0, 0, 0, 174, 173, 1, 0, 0,
		0, 175, 196, 1, 0, 0, 0, 176, 177, 10, 6, 0, 0, 177, 178, 7, 4, 0, 0, 178,
		195, 3, 32, 16, 7, 179, 180, 10, 5, 0, 0, 180, 181, 7, 5, 0, 0, 181, 195,
		3, 32, 16, 6, 182, 183, 10, 4, 0, 0, 183, 184, 7, 6, 0, 0, 184, 195, 3,
		32, 16, 5, 185, 186, 10, 3, 0, 0, 186, 187, 5, 21, 0, 0, 187, 195, 3, 32,
		16, 4, 188, 189, 10, 2, 0, 0, 189, 190, 5, 22, 0, 0, 190, 195, 3, 32, 16,
		3, 191, 192, 10, 1, 0, 0, 192, 193, 5, 23, 0, 0, 193, 195, 5, 40, 0, 0,
		194, 176, 1, 0, 0, 0, 194, 179, 1, 0, 0, 0, 194, 182, 1, 0, 0, 0, 194,
		185, 1, 0, 0, 0, 194, 188, 1, 0, 0, 0, 194, 191, 1, 0, 0, 0, 195, 198,
		1, 0, 0, 0, 196, 194, 1, 0, 0, 0, 196, 197, 1, 0, 0, 0, 197, 33, 1, 0,
		0, 0, 198, 196, 1, 0, 0, 0, 199, 206, 3, 38, 19, 0, 200, 206, 3, 48, 24,
		0, 201, 202, 5, 24, 0, 0, 202, 203, 3, 32, 16, 0, 203, 204, 5, 25, 0, 0,
		204, 206, 1, 0, 0, 0, 205, 199, 1, 0, 0, 0, 205, 200, 1, 0, 0, 0, 205,
		201, 1, 0, 0, 0, 206, 35, 1, 0, 0, 0, 207, 208, 5, 43, 0, 0, 208, 212,
		3, 32, 16, 0, 209, 210, 5, 44, 0, 0, 210, 212, 3, 62, 31, 0, 211, 207,
		1, 0, 0, 0, 211, 209, 1, 0, 0, 0, 212, 37, 1, 0, 0, 0, 213, 218, 3, 40,
		20, 0, 214, 218, 3, 42, 21, 0, 215, 218, 3, 44, 22, 0, 216, 218, 3, 46,
		23, 0, 217, 213, 1, 0, 0, 0, 217, 214, 1, 0, 0, 0, 217, 215, 1, 0, 0, 0,
		217, 216, 1, 0, 0, 0, 218, 39, 1, 0, 0, 0, 219, 220, 7, 7, 0, 0, 220, 41,
		1, 0, 0, 0, 221, 223, 5, 14, 0, 0, 222, 221, 1, 0, 0, 0, 222, 223, 1, 0,
		0, 0, 223, 224, 1, 0, 0, 0, 224, 225, 5, 41, 0, 0, 225, 43, 1, 0, 0, 0,
		226, 228, 5, 14, 0, 0, 227, 226, 1, 0, 0, 0, 227, 228, 1, 0, 0, 0, 228,
		229, 1, 0, 0, 0, 229, 230, 5, 41, 0, 0, 230, 231, 5, 42, 0, 0, 231, 232,
		5, 41, 0, 0, 232, 45, 1, 0, 0, 0, 233, 234, 5, 39, 0, 0, 234, 47, 1, 0,
		0, 0, 235, 248, 3, 62, 31, 0, 236, 238, 3, 62, 31, 0, 237, 239, 3, 52,
		26, 0, 238, 237, 1, 0, 0, 0, 239, 240, 1, 0, 0, 0, 240, 238, 1, 0, 0, 0,
		240, 241, 1, 0, 0, 0, 241, 248, 1, 0, 0, 0, 242, 244, 3, 52, 26, 0, 243,
		242, 1, 0, 0, 0, 244, 245, 1, 0, 0, 0, 245, 243, 1, 0, 0, 0, 245, 246,
		1, 0, 0, 0, 246, 248, 1, 0, 0, 0, 247, 235, 1, 0, 0, 0, 247, 236, 1, 0,
		0, 0, 247, 243, 1, 0, 0, 0, 248, 251, 1, 0, 0, 0, 249, 251, 3, 50, 25,
		0, 250, 247, 1, 0, 0, 0, 250, 249, 1, 0, 0, 0, 251, 49, 1, 0, 0, 0, 252,
		257, 5, 42, 0, 0, 253, 255, 3, 56, 28, 0, 254, 256, 3, 48, 24, 0, 255,
		254, 1, 0, 0, 0, 255, 256, 1, 0, 0, 0, 256, 258, 1, 0, 0, 0, 257, 253,
		1, 0, 0, 0, 257, 258, 1, 0, 0, 0, 258, 51, 1, 0, 0, 0, 259, 264, 3, 54,
		27, 0, 260, 264, 3, 56, 28, 0, 261, 264, 3, 58, 29, 0, 262, 264, 3, 60,
		30, 0, 263, 259, 1, 0, 0, 0, 263, 260, 1, 0, 0, 0, 263, 261, 1, 0, 0, 0,
		263, 262, 1, 0, 0, 0, 264, 53, 1, 0, 0, 0, 265, 266, 5, 42, 0, 0, 266,
		267, 3, 0, 0, 0, 267, 55, 1, 0, 0, 0, 268, 269, 5, 28, 0, 0, 269, 270,
		3, 32, 16, 0, 270, 271, 5, 29, 0, 0, 271, 57, 1, 0, 0, 0, 272, 274, 5,
		28, 0, 0, 273, 275, 3, 32, 16, 0, 274, 273, 1, 0, 0, 0, 274, 275, 1, 0,
		0, 0, 275, 276, 1, 0, 0, 0, 276, 278, 5, 8, 0, 0, 277, 279, 3, 32, 16,
		0, 278, 277, 1, 0, 0, 0, 278, 279, 1, 0, 0, 0, 279, 280, 1, 0, 0, 0, 280,
		281, 5, 29, 0, 0, 281, 59, 1, 0, 0, 0, 282, 283, 5, 24, 0, 0, 283, 284,
		5, 25, 0, 0, 284, 61, 1, 0, 0, 0, 285, 286, 3, 0, 0, 0, 286, 63, 1, 0,
		0, 0, 28, 79, 85, 104, 117, 121, 129, 132, 145, 148, 159, 162, 174, 194,
		196, 205, 211, 217, 222, 227, 240, 245, 247, 250, 255, 257, 263, 274, 278,
	}
	deserializer := antlr.NewATNDeserializer(nil)
	staticData.atn = deserializer.Deserialize(staticData.serializedATN)
	atn := staticData.atn
	staticData.decisionToDFA = make([]*antlr.DFA, len(atn.DecisionToState))
	decisionToDFA := staticData.decisionToDFA
	for index, state := range atn.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(state, index)
	}
}

// GoGenTemplateParserInit initializes any static state used to implement GoGenTemplateParser. By default the
// static state used to implement the parser is lazily initialized during the first call to
// NewGoGenTemplateParser(). You can call this function if you wish to initialize the static state ahead
// of time.
func GoGenTemplateParserInit() {
	staticData := &GoGenTemplateParserStaticData
	staticData.once.Do(gogentemplateParserInit)
}

// NewGoGenTemplateParser produces a new parser instance for the optional input antlr.TokenStream.
func NewGoGenTemplateParser(input antlr.TokenStream) *GoGenTemplateParser {
	GoGenTemplateParserInit()
	this := new(GoGenTemplateParser)
	this.BaseParser = antlr.NewBaseParser(input)
	staticData := &GoGenTemplateParserStaticData
	this.Interpreter = antlr.NewParserATNSimulator(this, staticData.atn, staticData.decisionToDFA, staticData.PredictionContextCache)
	this.RuleNames = staticData.RuleNames
	this.LiteralNames = staticData.LiteralNames
	this.SymbolicNames = staticData.SymbolicNames
	this.GrammarFileName = "GoGenTemplate.g4"

	return this
}

// GoGenTemplateParser tokens.
const (
	GoGenTemplateParserEOF        = antlr.TokenEOF
	GoGenTemplateParserT__0       = 1
	GoGenTemplateParserT__1       = 2
	GoGenTemplateParserT__2       = 3
	GoGenTemplateParserT__3       = 4
	GoGenTemplateParserT__4       = 5
	GoGenTemplateParserT__5       = 6
	GoGenTemplateParserT__6       = 7
	GoGenTemplateParserT__7       = 8
	GoGenTemplateParserT__8       = 9
	GoGenTemplateParserT__9       = 10
	GoGenTemplateParserT__10      = 11
	GoGenTemplateParserT__11      = 12
	GoGenTemplateParserT__12      = 13
	GoGenTemplateParserT__13      = 14
	GoGenTemplateParserT__14      = 15
	GoGenTemplateParserT__15      = 16
	GoGenTemplateParserT__16      = 17
	GoGenTemplateParserT__17      = 18
	GoGenTemplateParserT__18      = 19
	GoGenTemplateParserT__19      = 20
	GoGenTemplateParserT__20      = 21
	GoGenTemplateParserT__21      = 22
	GoGenTemplateParserT__22      = 23
	GoGenTemplateParserT__23      = 24
	GoGenTemplateParserT__24      = 25
	GoGenTemplateParserT__25      = 26
	GoGenTemplateParserT__26      = 27
	GoGenTemplateParserT__27      = 28
	GoGenTemplateParserT__28      = 29
	GoGenTemplateParserFOR        = 30
	GoGenTemplateParserIN         = 31
	GoGenTemplateParserEND        = 32
	GoGenTemplateParserIF         = 33
	GoGenTemplateParserINDENT     = 34
	GoGenTemplateParserELSE       = 35
	GoGenTemplateParserFIRST      = 36
	GoGenTemplateParserLAST       = 37
	GoGenTemplateParserEVEN       = 38
	GoGenTemplateParserSTRING     = 39
	GoGenTemplateParserIDENTIFIER = 40
	GoGenTemplateParserINT        = 41
	GoGenTemplateParserDOT        = 42
	GoGenTemplateParserNOT        = 43
	GoGenTemplateParserEXISTS     = 44
	GoGenTemplateParserWS         = 45
)

// GoGenTemplateParser rules.
const (
	GoGenTemplateParserRULE_identifier             = 0
	GoGenTemplateParserRULE_opn                    = 1
	GoGenTemplateParserRULE_cls                    = 2
	GoGenTemplateParserRULE_end                    = 3
	GoGenTemplateParserRULE_forBlock               = 4
	GoGenTemplateParserRULE_ifBlock                = 5
	GoGenTemplateParserRULE_elseBlock              = 6
	GoGenTemplateParserRULE_indentBlock            = 7
	GoGenTemplateParserRULE_echo                   = 8
	GoGenTemplateParserRULE_view                   = 9
	GoGenTemplateParserRULE_viewParam              = 10
	GoGenTemplateParserRULE_visitDefaultParameters = 11
	GoGenTemplateParserRULE_defaultParam           = 12
	GoGenTemplateParserRULE_importParameters       = 13
	GoGenTemplateParserRULE_importParameter        = 14
	GoGenTemplateParserRULE_rootExpression         = 15
	GoGenTemplateParserRULE_expression             = 16
	GoGenTemplateParserRULE_operand                = 17
	GoGenTemplateParserRULE_unaryExpression        = 18
	GoGenTemplateParserRULE_literal                = 19
	GoGenTemplateParserRULE_boolLiteral            = 20
	GoGenTemplateParserRULE_intLiteral             = 21
	GoGenTemplateParserRULE_floatLiteral           = 22
	GoGenTemplateParserRULE_stringLiteral          = 23
	GoGenTemplateParserRULE_selectorChain          = 24
	GoGenTemplateParserRULE_rootSelector           = 25
	GoGenTemplateParserRULE_selector               = 26
	GoGenTemplateParserRULE_fieldSelector          = 27
	GoGenTemplateParserRULE_index                  = 28
	GoGenTemplateParserRULE_slice                  = 29
	GoGenTemplateParserRULE_call                   = 30
	GoGenTemplateParserRULE_variable               = 31
)

// IIdentifierContext is an interface to support dynamic dispatch.
type IIdentifierContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	IDENTIFIER() antlr.TerminalNode
	FOR() antlr.TerminalNode
	IN() antlr.TerminalNode
	END() antlr.TerminalNode
	IF() antlr.TerminalNode
	ELSE() antlr.TerminalNode
	FIRST() antlr.TerminalNode
	LAST() antlr.TerminalNode
	EVEN() antlr.TerminalNode
	INDENT() antlr.TerminalNode

	// IsIdentifierContext differentiates from other interfaces.
	IsIdentifierContext()
}

type IdentifierContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIdentifierContext() *IdentifierContext {
	var p = new(IdentifierContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_identifier
	return p
}

func InitEmptyIdentifierContext(p *IdentifierContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_identifier
}

func (*IdentifierContext) IsIdentifierContext() {}

func NewIdentifierContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IdentifierContext {
	var p = new(IdentifierContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_identifier

	return p
}

func (s *IdentifierContext) GetParser() antlr.Parser { return s.parser }

func (s *IdentifierContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIDENTIFIER, 0)
}

func (s *IdentifierContext) FOR() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserFOR, 0)
}

func (s *IdentifierContext) IN() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIN, 0)
}

func (s *IdentifierContext) END() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserEND, 0)
}

func (s *IdentifierContext) IF() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIF, 0)
}

func (s *IdentifierContext) ELSE() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserELSE, 0)
}

func (s *IdentifierContext) FIRST() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserFIRST, 0)
}

func (s *IdentifierContext) LAST() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserLAST, 0)
}

func (s *IdentifierContext) EVEN() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserEVEN, 0)
}

func (s *IdentifierContext) INDENT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserINDENT, 0)
}

func (s *IdentifierContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdentifierContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IdentifierContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitIdentifier(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Identifier() (localctx IIdentifierContext) {
	localctx = NewIdentifierContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, GoGenTemplateParserRULE_identifier)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(64)
		_la = p.GetTokenStream().LA(1)

		if !((int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&1648193699840) != 0) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IOpnContext is an interface to support dynamic dispatch.
type IOpnContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsOpnContext differentiates from other interfaces.
	IsOpnContext()
}

type OpnContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyOpnContext() *OpnContext {
	var p = new(OpnContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_opn
	return p
}

func InitEmptyOpnContext(p *OpnContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_opn
}

func (*OpnContext) IsOpnContext() {}

func NewOpnContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *OpnContext {
	var p = new(OpnContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_opn

	return p
}

func (s *OpnContext) GetParser() antlr.Parser { return s.parser }
func (s *OpnContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *OpnContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *OpnContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitOpn(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Opn() (localctx IOpnContext) {
	localctx = NewOpnContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, GoGenTemplateParserRULE_opn)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(66)
		_la = p.GetTokenStream().LA(1)

		if !(_la == GoGenTemplateParserT__0 || _la == GoGenTemplateParserT__1) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IClsContext is an interface to support dynamic dispatch.
type IClsContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsClsContext differentiates from other interfaces.
	IsClsContext()
}

type ClsContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyClsContext() *ClsContext {
	var p = new(ClsContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_cls
	return p
}

func InitEmptyClsContext(p *ClsContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_cls
}

func (*ClsContext) IsClsContext() {}

func NewClsContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ClsContext {
	var p = new(ClsContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_cls

	return p
}

func (s *ClsContext) GetParser() antlr.Parser { return s.parser }
func (s *ClsContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ClsContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ClsContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitCls(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Cls() (localctx IClsContext) {
	localctx = NewClsContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, GoGenTemplateParserRULE_cls)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(68)
		_la = p.GetTokenStream().LA(1)

		if !(_la == GoGenTemplateParserT__2 || _la == GoGenTemplateParserT__3) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IEndContext is an interface to support dynamic dispatch.
type IEndContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Opn() IOpnContext
	END() antlr.TerminalNode
	Cls() IClsContext

	// IsEndContext differentiates from other interfaces.
	IsEndContext()
}

type EndContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEndContext() *EndContext {
	var p = new(EndContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_end
	return p
}

func InitEmptyEndContext(p *EndContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_end
}

func (*EndContext) IsEndContext() {}

func NewEndContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EndContext {
	var p = new(EndContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_end

	return p
}

func (s *EndContext) GetParser() antlr.Parser { return s.parser }

func (s *EndContext) Opn() IOpnContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IOpnContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IOpnContext)
}

func (s *EndContext) END() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserEND, 0)
}

func (s *EndContext) Cls() IClsContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IClsContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IClsContext)
}

func (s *EndContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EndContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EndContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitEnd(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) End() (localctx IEndContext) {
	localctx = NewEndContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, GoGenTemplateParserRULE_end)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(70)
		p.Opn()
	}
	{
		p.SetState(71)
		p.Match(GoGenTemplateParserEND)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(72)
		p.Cls()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IForBlockContext is an interface to support dynamic dispatch.
type IForBlockContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetKey returns the key rule contexts.
	GetKey() IIdentifierContext

	// GetValue returns the value rule contexts.
	GetValue() IIdentifierContext

	// SetKey sets the key rule contexts.
	SetKey(IIdentifierContext)

	// SetValue sets the value rule contexts.
	SetValue(IIdentifierContext)

	// Getter signatures
	Opn() IOpnContext
	FOR() antlr.TerminalNode
	IN() antlr.TerminalNode
	Expression() IExpressionContext
	Cls() IClsContext
	AllIdentifier() []IIdentifierContext
	Identifier(i int) IIdentifierContext
	AllFIRST() []antlr.TerminalNode
	FIRST(i int) antlr.TerminalNode
	AllLAST() []antlr.TerminalNode
	LAST(i int) antlr.TerminalNode
	AllEVEN() []antlr.TerminalNode
	EVEN(i int) antlr.TerminalNode

	// IsForBlockContext differentiates from other interfaces.
	IsForBlockContext()
}

type ForBlockContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
	key    IIdentifierContext
	value  IIdentifierContext
}

func NewEmptyForBlockContext() *ForBlockContext {
	var p = new(ForBlockContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_forBlock
	return p
}

func InitEmptyForBlockContext(p *ForBlockContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_forBlock
}

func (*ForBlockContext) IsForBlockContext() {}

func NewForBlockContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ForBlockContext {
	var p = new(ForBlockContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_forBlock

	return p
}

func (s *ForBlockContext) GetParser() antlr.Parser { return s.parser }

func (s *ForBlockContext) GetKey() IIdentifierContext { return s.key }

func (s *ForBlockContext) GetValue() IIdentifierContext { return s.value }

func (s *ForBlockContext) SetKey(v IIdentifierContext) { s.key = v }

func (s *ForBlockContext) SetValue(v IIdentifierContext) { s.value = v }

func (s *ForBlockContext) Opn() IOpnContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IOpnContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IOpnContext)
}

func (s *ForBlockContext) FOR() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserFOR, 0)
}

func (s *ForBlockContext) IN() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIN, 0)
}

func (s *ForBlockContext) Expression() IExpressionContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ForBlockContext) Cls() IClsContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IClsContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IClsContext)
}

func (s *ForBlockContext) AllIdentifier() []IIdentifierContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IIdentifierContext); ok {
			len++
		}
	}

	tst := make([]IIdentifierContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IIdentifierContext); ok {
			tst[i] = t.(IIdentifierContext)
			i++
		}
	}

	return tst
}

func (s *ForBlockContext) Identifier(i int) IIdentifierContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *ForBlockContext) AllFIRST() []antlr.TerminalNode {
	return s.GetTokens(GoGenTemplateParserFIRST)
}

func (s *ForBlockContext) FIRST(i int) antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserFIRST, i)
}

func (s *ForBlockContext) AllLAST() []antlr.TerminalNode {
	return s.GetTokens(GoGenTemplateParserLAST)
}

func (s *ForBlockContext) LAST(i int) antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserLAST, i)
}

func (s *ForBlockContext) AllEVEN() []antlr.TerminalNode {
	return s.GetTokens(GoGenTemplateParserEVEN)
}

func (s *ForBlockContext) EVEN(i int) antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserEVEN, i)
}

func (s *ForBlockContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ForBlockContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ForBlockContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitForBlock(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) ForBlock() (localctx IForBlockContext) {
	localctx = NewForBlockContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, GoGenTemplateParserRULE_forBlock)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(74)
		p.Opn()
	}
	{
		p.SetState(75)
		p.Match(GoGenTemplateParserFOR)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(76)

		var _x = p.Identifier()

		localctx.(*ForBlockContext).key = _x
	}
	p.SetState(79)
	p.GetErrorHandler().Sync(p)

	if p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 0, p.GetParserRuleContext()) == 1 {
		{
			p.SetState(77)
			p.Match(GoGenTemplateParserT__4)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(78)

			var _x = p.Identifier()

			localctx.(*ForBlockContext).value = _x
		}

	} else if p.HasError() { // JIM
		goto errorExit
	}
	p.SetState(85)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == GoGenTemplateParserT__4 {
		{
			p.SetState(81)
			p.Match(GoGenTemplateParserT__4)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(82)
			_la = p.GetTokenStream().LA(1)

			if !((int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&481036337152) != 0) {
				p.GetErrorHandler().RecoverInline(p)
			} else {
				p.GetErrorHandler().ReportMatch(p)
				p.Consume()
			}
		}

		p.SetState(87)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(88)
		p.Match(GoGenTemplateParserIN)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(89)
		p.expression(0)
	}
	{
		p.SetState(90)
		p.Cls()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IIfBlockContext is an interface to support dynamic dispatch.
type IIfBlockContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Opn() IOpnContext
	IF() antlr.TerminalNode
	Expression() IExpressionContext
	Cls() IClsContext

	// IsIfBlockContext differentiates from other interfaces.
	IsIfBlockContext()
}

type IfBlockContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIfBlockContext() *IfBlockContext {
	var p = new(IfBlockContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_ifBlock
	return p
}

func InitEmptyIfBlockContext(p *IfBlockContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_ifBlock
}

func (*IfBlockContext) IsIfBlockContext() {}

func NewIfBlockContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IfBlockContext {
	var p = new(IfBlockContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_ifBlock

	return p
}

func (s *IfBlockContext) GetParser() antlr.Parser { return s.parser }

func (s *IfBlockContext) Opn() IOpnContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IOpnContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IOpnContext)
}

func (s *IfBlockContext) IF() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIF, 0)
}

func (s *IfBlockContext) Expression() IExpressionContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *IfBlockContext) Cls() IClsContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IClsContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IClsContext)
}

func (s *IfBlockContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IfBlockContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IfBlockContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitIfBlock(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) IfBlock() (localctx IIfBlockContext) {
	localctx = NewIfBlockContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, GoGenTemplateParserRULE_ifBlock)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(92)
		p.Opn()
	}
	{
		p.SetState(93)
		p.Match(GoGenTemplateParserIF)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(94)
		p.expression(0)
	}
	{
		p.SetState(95)
		p.Cls()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IElseBlockContext is an interface to support dynamic dispatch.
type IElseBlockContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Opn() IOpnContext
	ELSE() antlr.TerminalNode
	Cls() IClsContext

	// IsElseBlockContext differentiates from other interfaces.
	IsElseBlockContext()
}

type ElseBlockContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyElseBlockContext() *ElseBlockContext {
	var p = new(ElseBlockContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_elseBlock
	return p
}

func InitEmptyElseBlockContext(p *ElseBlockContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_elseBlock
}

func (*ElseBlockContext) IsElseBlockContext() {}

func NewElseBlockContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ElseBlockContext {
	var p = new(ElseBlockContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_elseBlock

	return p
}

func (s *ElseBlockContext) GetParser() antlr.Parser { return s.parser }

func (s *ElseBlockContext) Opn() IOpnContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IOpnContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IOpnContext)
}

func (s *ElseBlockContext) ELSE() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserELSE, 0)
}

func (s *ElseBlockContext) Cls() IClsContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IClsContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IClsContext)
}

func (s *ElseBlockContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ElseBlockContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ElseBlockContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitElseBlock(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) ElseBlock() (localctx IElseBlockContext) {
	localctx = NewElseBlockContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 12, GoGenTemplateParserRULE_elseBlock)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(97)
		p.Opn()
	}
	{
		p.SetState(98)
		p.Match(GoGenTemplateParserELSE)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(99)
		p.Cls()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IIndentBlockContext is an interface to support dynamic dispatch.
type IIndentBlockContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Opn() IOpnContext
	INDENT() antlr.TerminalNode
	Cls() IClsContext
	Expression() IExpressionContext

	// IsIndentBlockContext differentiates from other interfaces.
	IsIndentBlockContext()
}

type IndentBlockContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIndentBlockContext() *IndentBlockContext {
	var p = new(IndentBlockContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_indentBlock
	return p
}

func InitEmptyIndentBlockContext(p *IndentBlockContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_indentBlock
}

func (*IndentBlockContext) IsIndentBlockContext() {}

func NewIndentBlockContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IndentBlockContext {
	var p = new(IndentBlockContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_indentBlock

	return p
}

func (s *IndentBlockContext) GetParser() antlr.Parser { return s.parser }

func (s *IndentBlockContext) Opn() IOpnContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IOpnContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IOpnContext)
}

func (s *IndentBlockContext) INDENT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserINDENT, 0)
}

func (s *IndentBlockContext) Cls() IClsContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IClsContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IClsContext)
}

func (s *IndentBlockContext) Expression() IExpressionContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *IndentBlockContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IndentBlockContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IndentBlockContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitIndentBlock(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) IndentBlock() (localctx IIndentBlockContext) {
	localctx = NewIndentBlockContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 14, GoGenTemplateParserRULE_indentBlock)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(101)
		p.Opn()
	}
	{
		p.SetState(102)
		p.Match(GoGenTemplateParserINDENT)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(104)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&35183784902656) != 0 {
		{
			p.SetState(103)
			p.expression(0)
		}

	}
	{
		p.SetState(106)
		p.Cls()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IEchoContext is an interface to support dynamic dispatch.
type IEchoContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Opn() IOpnContext
	Expression() IExpressionContext
	Cls() IClsContext

	// IsEchoContext differentiates from other interfaces.
	IsEchoContext()
}

type EchoContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEchoContext() *EchoContext {
	var p = new(EchoContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_echo
	return p
}

func InitEmptyEchoContext(p *EchoContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_echo
}

func (*EchoContext) IsEchoContext() {}

func NewEchoContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EchoContext {
	var p = new(EchoContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_echo

	return p
}

func (s *EchoContext) GetParser() antlr.Parser { return s.parser }

func (s *EchoContext) Opn() IOpnContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IOpnContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IOpnContext)
}

func (s *EchoContext) Expression() IExpressionContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *EchoContext) Cls() IClsContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IClsContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IClsContext)
}

func (s *EchoContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EchoContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EchoContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitEcho(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Echo() (localctx IEchoContext) {
	localctx = NewEchoContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 16, GoGenTemplateParserRULE_echo)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(108)
		p.Match(GoGenTemplateParserT__5)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(109)
		p.Opn()
	}
	{
		p.SetState(110)
		p.expression(0)
	}
	{
		p.SetState(111)
		p.Cls()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IViewContext is an interface to support dynamic dispatch.
type IViewContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetPkg returns the pkg token.
	GetPkg() antlr.Token

	// GetName returns the name token.
	GetName() antlr.Token

	// SetPkg sets the pkg token.
	SetPkg(antlr.Token)

	// SetName sets the name token.
	SetName(antlr.Token)

	// Getter signatures
	Opn() IOpnContext
	Expression() IExpressionContext
	Cls() IClsContext
	AllViewParam() []IViewParamContext
	ViewParam(i int) IViewParamContext
	AllIDENTIFIER() []antlr.TerminalNode
	IDENTIFIER(i int) antlr.TerminalNode
	DOT() antlr.TerminalNode

	// IsViewContext differentiates from other interfaces.
	IsViewContext()
}

type ViewContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
	pkg    antlr.Token
	name   antlr.Token
}

func NewEmptyViewContext() *ViewContext {
	var p = new(ViewContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_view
	return p
}

func InitEmptyViewContext(p *ViewContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_view
}

func (*ViewContext) IsViewContext() {}

func NewViewContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ViewContext {
	var p = new(ViewContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_view

	return p
}

func (s *ViewContext) GetParser() antlr.Parser { return s.parser }

func (s *ViewContext) GetPkg() antlr.Token { return s.pkg }

func (s *ViewContext) GetName() antlr.Token { return s.name }

func (s *ViewContext) SetPkg(v antlr.Token) { s.pkg = v }

func (s *ViewContext) SetName(v antlr.Token) { s.name = v }

func (s *ViewContext) Opn() IOpnContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IOpnContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IOpnContext)
}

func (s *ViewContext) Expression() IExpressionContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ViewContext) Cls() IClsContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IClsContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IClsContext)
}

func (s *ViewContext) AllViewParam() []IViewParamContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IViewParamContext); ok {
			len++
		}
	}

	tst := make([]IViewParamContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IViewParamContext); ok {
			tst[i] = t.(IViewParamContext)
			i++
		}
	}

	return tst
}

func (s *ViewContext) ViewParam(i int) IViewParamContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IViewParamContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IViewParamContext)
}

func (s *ViewContext) AllIDENTIFIER() []antlr.TerminalNode {
	return s.GetTokens(GoGenTemplateParserIDENTIFIER)
}

func (s *ViewContext) IDENTIFIER(i int) antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIDENTIFIER, i)
}

func (s *ViewContext) DOT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserDOT, 0)
}

func (s *ViewContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ViewContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ViewContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitView(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) View() (localctx IViewContext) {
	localctx = NewViewContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 18, GoGenTemplateParserRULE_view)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(113)
		p.Match(GoGenTemplateParserT__6)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(114)
		p.Opn()
	}
	p.SetState(121)
	p.GetErrorHandler().Sync(p)

	if p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 4, p.GetParserRuleContext()) == 1 {
		p.SetState(117)
		p.GetErrorHandler().Sync(p)

		if p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 3, p.GetParserRuleContext()) == 1 {
			{
				p.SetState(115)

				var _m = p.Match(GoGenTemplateParserIDENTIFIER)

				localctx.(*ViewContext).pkg = _m
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}
			{
				p.SetState(116)
				p.Match(GoGenTemplateParserDOT)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		} else if p.HasError() { // JIM
			goto errorExit
		}
		{
			p.SetState(119)

			var _m = p.Match(GoGenTemplateParserIDENTIFIER)

			localctx.(*ViewContext).name = _m
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(120)
			p.Match(GoGenTemplateParserT__7)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	} else if p.HasError() { // JIM
		goto errorExit
	}
	{
		p.SetState(123)
		p.expression(0)
	}
	p.SetState(132)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == GoGenTemplateParserIDENTIFIER {
		{
			p.SetState(124)
			p.ViewParam()
		}
		p.SetState(129)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		for _la == GoGenTemplateParserT__4 {
			{
				p.SetState(125)
				p.Match(GoGenTemplateParserT__4)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}
			{
				p.SetState(126)
				p.ViewParam()
			}

			p.SetState(131)
			p.GetErrorHandler().Sync(p)
			if p.HasError() {
				goto errorExit
			}
			_la = p.GetTokenStream().LA(1)
		}

	}
	{
		p.SetState(134)
		p.Cls()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IViewParamContext is an interface to support dynamic dispatch.
type IViewParamContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	IDENTIFIER() antlr.TerminalNode
	Expression() IExpressionContext

	// IsViewParamContext differentiates from other interfaces.
	IsViewParamContext()
}

type ViewParamContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyViewParamContext() *ViewParamContext {
	var p = new(ViewParamContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_viewParam
	return p
}

func InitEmptyViewParamContext(p *ViewParamContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_viewParam
}

func (*ViewParamContext) IsViewParamContext() {}

func NewViewParamContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ViewParamContext {
	var p = new(ViewParamContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_viewParam

	return p
}

func (s *ViewParamContext) GetParser() antlr.Parser { return s.parser }

func (s *ViewParamContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIDENTIFIER, 0)
}

func (s *ViewParamContext) Expression() IExpressionContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ViewParamContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ViewParamContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ViewParamContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitViewParam(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) ViewParam() (localctx IViewParamContext) {
	localctx = NewViewParamContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 20, GoGenTemplateParserRULE_viewParam)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(136)
		p.Match(GoGenTemplateParserIDENTIFIER)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(137)
		p.Match(GoGenTemplateParserT__8)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(138)
		p.expression(0)
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IVisitDefaultParametersContext is an interface to support dynamic dispatch.
type IVisitDefaultParametersContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	AllDefaultParam() []IDefaultParamContext
	DefaultParam(i int) IDefaultParamContext

	// IsVisitDefaultParametersContext differentiates from other interfaces.
	IsVisitDefaultParametersContext()
}

type VisitDefaultParametersContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyVisitDefaultParametersContext() *VisitDefaultParametersContext {
	var p = new(VisitDefaultParametersContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_visitDefaultParameters
	return p
}

func InitEmptyVisitDefaultParametersContext(p *VisitDefaultParametersContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_visitDefaultParameters
}

func (*VisitDefaultParametersContext) IsVisitDefaultParametersContext() {}

func NewVisitDefaultParametersContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *VisitDefaultParametersContext {
	var p = new(VisitDefaultParametersContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_visitDefaultParameters

	return p
}

func (s *VisitDefaultParametersContext) GetParser() antlr.Parser { return s.parser }

func (s *VisitDefaultParametersContext) AllDefaultParam() []IDefaultParamContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IDefaultParamContext); ok {
			len++
		}
	}

	tst := make([]IDefaultParamContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IDefaultParamContext); ok {
			tst[i] = t.(IDefaultParamContext)
			i++
		}
	}

	return tst
}

func (s *VisitDefaultParametersContext) DefaultParam(i int) IDefaultParamContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IDefaultParamContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IDefaultParamContext)
}

func (s *VisitDefaultParametersContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *VisitDefaultParametersContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *VisitDefaultParametersContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitVisitDefaultParameters(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) VisitDefaultParameters() (localctx IVisitDefaultParametersContext) {
	localctx = NewVisitDefaultParametersContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 22, GoGenTemplateParserRULE_visitDefaultParameters)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(148)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == GoGenTemplateParserIDENTIFIER {
		{
			p.SetState(140)
			p.DefaultParam()
		}
		p.SetState(145)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		for _la == GoGenTemplateParserT__4 {
			{
				p.SetState(141)
				p.Match(GoGenTemplateParserT__4)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}
			{
				p.SetState(142)
				p.DefaultParam()
			}

			p.SetState(147)
			p.GetErrorHandler().Sync(p)
			if p.HasError() {
				goto errorExit
			}
			_la = p.GetTokenStream().LA(1)
		}

	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IDefaultParamContext is an interface to support dynamic dispatch.
type IDefaultParamContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	IDENTIFIER() antlr.TerminalNode
	Literal() ILiteralContext

	// IsDefaultParamContext differentiates from other interfaces.
	IsDefaultParamContext()
}

type DefaultParamContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyDefaultParamContext() *DefaultParamContext {
	var p = new(DefaultParamContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_defaultParam
	return p
}

func InitEmptyDefaultParamContext(p *DefaultParamContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_defaultParam
}

func (*DefaultParamContext) IsDefaultParamContext() {}

func NewDefaultParamContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *DefaultParamContext {
	var p = new(DefaultParamContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_defaultParam

	return p
}

func (s *DefaultParamContext) GetParser() antlr.Parser { return s.parser }

func (s *DefaultParamContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIDENTIFIER, 0)
}

func (s *DefaultParamContext) Literal() ILiteralContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ILiteralContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ILiteralContext)
}

func (s *DefaultParamContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DefaultParamContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *DefaultParamContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitDefaultParam(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) DefaultParam() (localctx IDefaultParamContext) {
	localctx = NewDefaultParamContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 24, GoGenTemplateParserRULE_defaultParam)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(150)
		p.Match(GoGenTemplateParserIDENTIFIER)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(151)
		p.Match(GoGenTemplateParserT__8)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(152)
		p.Literal()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IImportParametersContext is an interface to support dynamic dispatch.
type IImportParametersContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	AllImportParameter() []IImportParameterContext
	ImportParameter(i int) IImportParameterContext

	// IsImportParametersContext differentiates from other interfaces.
	IsImportParametersContext()
}

type ImportParametersContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyImportParametersContext() *ImportParametersContext {
	var p = new(ImportParametersContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_importParameters
	return p
}

func InitEmptyImportParametersContext(p *ImportParametersContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_importParameters
}

func (*ImportParametersContext) IsImportParametersContext() {}

func NewImportParametersContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ImportParametersContext {
	var p = new(ImportParametersContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_importParameters

	return p
}

func (s *ImportParametersContext) GetParser() antlr.Parser { return s.parser }

func (s *ImportParametersContext) AllImportParameter() []IImportParameterContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IImportParameterContext); ok {
			len++
		}
	}

	tst := make([]IImportParameterContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IImportParameterContext); ok {
			tst[i] = t.(IImportParameterContext)
			i++
		}
	}

	return tst
}

func (s *ImportParametersContext) ImportParameter(i int) IImportParameterContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IImportParameterContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IImportParameterContext)
}

func (s *ImportParametersContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ImportParametersContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ImportParametersContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitImportParameters(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) ImportParameters() (localctx IImportParametersContext) {
	localctx = NewImportParametersContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 26, GoGenTemplateParserRULE_importParameters)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(162)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == GoGenTemplateParserIDENTIFIER {
		{
			p.SetState(154)
			p.ImportParameter()
		}
		p.SetState(159)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		for _la == GoGenTemplateParserT__4 {
			{
				p.SetState(155)
				p.Match(GoGenTemplateParserT__4)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}
			{
				p.SetState(156)
				p.ImportParameter()
			}

			p.SetState(161)
			p.GetErrorHandler().Sync(p)
			if p.HasError() {
				goto errorExit
			}
			_la = p.GetTokenStream().LA(1)
		}

	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IImportParameterContext is an interface to support dynamic dispatch.
type IImportParameterContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	IDENTIFIER() antlr.TerminalNode
	Expression() IExpressionContext

	// IsImportParameterContext differentiates from other interfaces.
	IsImportParameterContext()
}

type ImportParameterContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyImportParameterContext() *ImportParameterContext {
	var p = new(ImportParameterContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_importParameter
	return p
}

func InitEmptyImportParameterContext(p *ImportParameterContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_importParameter
}

func (*ImportParameterContext) IsImportParameterContext() {}

func NewImportParameterContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ImportParameterContext {
	var p = new(ImportParameterContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_importParameter

	return p
}

func (s *ImportParameterContext) GetParser() antlr.Parser { return s.parser }

func (s *ImportParameterContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIDENTIFIER, 0)
}

func (s *ImportParameterContext) Expression() IExpressionContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ImportParameterContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ImportParameterContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ImportParameterContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitImportParameter(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) ImportParameter() (localctx IImportParameterContext) {
	localctx = NewImportParameterContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 28, GoGenTemplateParserRULE_importParameter)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(164)
		p.Match(GoGenTemplateParserIDENTIFIER)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(165)
		p.Match(GoGenTemplateParserT__8)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(166)
		p.expression(0)
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IRootExpressionContext is an interface to support dynamic dispatch.
type IRootExpressionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Expression() IExpressionContext
	EOF() antlr.TerminalNode

	// IsRootExpressionContext differentiates from other interfaces.
	IsRootExpressionContext()
}

type RootExpressionContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyRootExpressionContext() *RootExpressionContext {
	var p = new(RootExpressionContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_rootExpression
	return p
}

func InitEmptyRootExpressionContext(p *RootExpressionContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_rootExpression
}

func (*RootExpressionContext) IsRootExpressionContext() {}

func NewRootExpressionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RootExpressionContext {
	var p = new(RootExpressionContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_rootExpression

	return p
}

func (s *RootExpressionContext) GetParser() antlr.Parser { return s.parser }

func (s *RootExpressionContext) Expression() IExpressionContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *RootExpressionContext) EOF() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserEOF, 0)
}

func (s *RootExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RootExpressionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RootExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitRootExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) RootExpression() (localctx IRootExpressionContext) {
	localctx = NewRootExpressionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 30, GoGenTemplateParserRULE_rootExpression)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(168)
		p.expression(0)
	}
	{
		p.SetState(169)
		p.Match(GoGenTemplateParserEOF)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IExpressionContext is an interface to support dynamic dispatch.
type IExpressionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetOperator returns the operator token.
	GetOperator() antlr.Token

	// GetFilter returns the filter token.
	GetFilter() antlr.Token

	// SetOperator sets the operator token.
	SetOperator(antlr.Token)

	// SetFilter sets the filter token.
	SetFilter(antlr.Token)

	// Getter signatures
	Operand() IOperandContext
	UnaryExpression() IUnaryExpressionContext
	AllExpression() []IExpressionContext
	Expression(i int) IExpressionContext
	IDENTIFIER() antlr.TerminalNode

	// IsExpressionContext differentiates from other interfaces.
	IsExpressionContext()
}

type ExpressionContext struct {
	antlr.BaseParserRuleContext
	parser   antlr.Parser
	operator antlr.Token
	filter   antlr.Token
}

func NewEmptyExpressionContext() *ExpressionContext {
	var p = new(ExpressionContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_expression
	return p
}

func InitEmptyExpressionContext(p *ExpressionContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_expression
}

func (*ExpressionContext) IsExpressionContext() {}

func NewExpressionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ExpressionContext {
	var p = new(ExpressionContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_expression

	return p
}

func (s *ExpressionContext) GetParser() antlr.Parser { return s.parser }

func (s *ExpressionContext) GetOperator() antlr.Token { return s.operator }

func (s *ExpressionContext) GetFilter() antlr.Token { return s.filter }

func (s *ExpressionContext) SetOperator(v antlr.Token) { s.operator = v }

func (s *ExpressionContext) SetFilter(v antlr.Token) { s.filter = v }

func (s *ExpressionContext) Operand() IOperandContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IOperandContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IOperandContext)
}

func (s *ExpressionContext) UnaryExpression() IUnaryExpressionContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IUnaryExpressionContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IUnaryExpressionContext)
}

func (s *ExpressionContext) AllExpression() []IExpressionContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExpressionContext); ok {
			len++
		}
	}

	tst := make([]IExpressionContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExpressionContext); ok {
			tst[i] = t.(IExpressionContext)
			i++
		}
	}

	return tst
}

func (s *ExpressionContext) Expression(i int) IExpressionContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ExpressionContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserIDENTIFIER, 0)
}

func (s *ExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ExpressionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Expression() (localctx IExpressionContext) {
	return p.expression(0)
}

func (p *GoGenTemplateParser) expression(_p int) (localctx IExpressionContext) {
	var _parentctx antlr.ParserRuleContext = p.GetParserRuleContext()

	_parentState := p.GetState()
	localctx = NewExpressionContext(p, p.GetParserRuleContext(), _parentState)
	var _prevctx IExpressionContext = localctx
	var _ antlr.ParserRuleContext = _prevctx // TODO: To prevent unused variable warning.
	_startState := 32
	p.EnterRecursionRule(localctx, 32, GoGenTemplateParserRULE_expression, _p)
	var _la int

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(174)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case GoGenTemplateParserT__13, GoGenTemplateParserT__23, GoGenTemplateParserT__25, GoGenTemplateParserT__26, GoGenTemplateParserT__27, GoGenTemplateParserFOR, GoGenTemplateParserIN, GoGenTemplateParserEND, GoGenTemplateParserIF, GoGenTemplateParserINDENT, GoGenTemplateParserELSE, GoGenTemplateParserFIRST, GoGenTemplateParserLAST, GoGenTemplateParserEVEN, GoGenTemplateParserSTRING, GoGenTemplateParserIDENTIFIER, GoGenTemplateParserINT, GoGenTemplateParserDOT:
		{
			p.SetState(172)
			p.Operand()
		}

	case GoGenTemplateParserNOT, GoGenTemplateParserEXISTS:
		{
			p.SetState(173)
			p.UnaryExpression()
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}
	p.GetParserRuleContext().SetStop(p.GetTokenStream().LT(-1))
	p.SetState(196)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 13, p.GetParserRuleContext())
	if p.HasError() {
		goto errorExit
	}
	for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1 {
			if p.GetParseListeners() != nil {
				p.TriggerExitRuleEvent()
			}
			_prevctx = localctx
			p.SetState(194)
			p.GetErrorHandler().Sync(p)
			if p.HasError() {
				goto errorExit
			}

			switch p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 12, p.GetParserRuleContext()) {
			case 1:
				localctx = NewExpressionContext(p, _parentctx, _parentState)
				p.PushNewRecursionContext(localctx, _startState, GoGenTemplateParserRULE_expression)
				p.SetState(176)

				if !(p.Precpred(p.GetParserRuleContext(), 6)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 6)", ""))
					goto errorExit
				}
				{
					p.SetState(177)

					var _lt = p.GetTokenStream().LT(1)

					localctx.(*ExpressionContext).operator = _lt

					_la = p.GetTokenStream().LA(1)

					if !((int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&7168) != 0) {
						var _ri = p.GetErrorHandler().RecoverInline(p)

						localctx.(*ExpressionContext).operator = _ri
					} else {
						p.GetErrorHandler().ReportMatch(p)
						p.Consume()
					}
				}
				{
					p.SetState(178)
					p.expression(7)
				}

			case 2:
				localctx = NewExpressionContext(p, _parentctx, _parentState)
				p.PushNewRecursionContext(localctx, _startState, GoGenTemplateParserRULE_expression)
				p.SetState(179)

				if !(p.Precpred(p.GetParserRuleContext(), 5)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 5)", ""))
					goto errorExit
				}
				{
					p.SetState(180)

					var _lt = p.GetTokenStream().LT(1)

					localctx.(*ExpressionContext).operator = _lt

					_la = p.GetTokenStream().LA(1)

					if !(_la == GoGenTemplateParserT__12 || _la == GoGenTemplateParserT__13) {
						var _ri = p.GetErrorHandler().RecoverInline(p)

						localctx.(*ExpressionContext).operator = _ri
					} else {
						p.GetErrorHandler().ReportMatch(p)
						p.Consume()
					}
				}
				{
					p.SetState(181)
					p.expression(6)
				}

			case 3:
				localctx = NewExpressionContext(p, _parentctx, _parentState)
				p.PushNewRecursionContext(localctx, _startState, GoGenTemplateParserRULE_expression)
				p.SetState(182)

				if !(p.Precpred(p.GetParserRuleContext(), 4)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 4)", ""))
					goto errorExit
				}
				{
					p.SetState(183)

					var _lt = p.GetTokenStream().LT(1)

					localctx.(*ExpressionContext).operator = _lt

					_la = p.GetTokenStream().LA(1)

					if !((int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&2064384) != 0) {
						var _ri = p.GetErrorHandler().RecoverInline(p)

						localctx.(*ExpressionContext).operator = _ri
					} else {
						p.GetErrorHandler().ReportMatch(p)
						p.Consume()
					}
				}
				{
					p.SetState(184)
					p.expression(5)
				}

			case 4:
				localctx = NewExpressionContext(p, _parentctx, _parentState)
				p.PushNewRecursionContext(localctx, _startState, GoGenTemplateParserRULE_expression)
				p.SetState(185)

				if !(p.Precpred(p.GetParserRuleContext(), 3)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 3)", ""))
					goto errorExit
				}
				{
					p.SetState(186)

					var _m = p.Match(GoGenTemplateParserT__20)

					localctx.(*ExpressionContext).operator = _m
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				{
					p.SetState(187)
					p.expression(4)
				}

			case 5:
				localctx = NewExpressionContext(p, _parentctx, _parentState)
				p.PushNewRecursionContext(localctx, _startState, GoGenTemplateParserRULE_expression)
				p.SetState(188)

				if !(p.Precpred(p.GetParserRuleContext(), 2)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 2)", ""))
					goto errorExit
				}
				{
					p.SetState(189)

					var _m = p.Match(GoGenTemplateParserT__21)

					localctx.(*ExpressionContext).operator = _m
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				{
					p.SetState(190)
					p.expression(3)
				}

			case 6:
				localctx = NewExpressionContext(p, _parentctx, _parentState)
				p.PushNewRecursionContext(localctx, _startState, GoGenTemplateParserRULE_expression)
				p.SetState(191)

				if !(p.Precpred(p.GetParserRuleContext(), 1)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 1)", ""))
					goto errorExit
				}
				{
					p.SetState(192)
					p.Match(GoGenTemplateParserT__22)
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				{
					p.SetState(193)

					var _m = p.Match(GoGenTemplateParserIDENTIFIER)

					localctx.(*ExpressionContext).filter = _m
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}

			case antlr.ATNInvalidAltNumber:
				goto errorExit
			}

		}
		p.SetState(198)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 13, p.GetParserRuleContext())
		if p.HasError() {
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.UnrollRecursionContexts(_parentctx)
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IOperandContext is an interface to support dynamic dispatch.
type IOperandContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Literal() ILiteralContext
	SelectorChain() ISelectorChainContext
	Expression() IExpressionContext

	// IsOperandContext differentiates from other interfaces.
	IsOperandContext()
}

type OperandContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyOperandContext() *OperandContext {
	var p = new(OperandContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_operand
	return p
}

func InitEmptyOperandContext(p *OperandContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_operand
}

func (*OperandContext) IsOperandContext() {}

func NewOperandContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *OperandContext {
	var p = new(OperandContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_operand

	return p
}

func (s *OperandContext) GetParser() antlr.Parser { return s.parser }

func (s *OperandContext) Literal() ILiteralContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ILiteralContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ILiteralContext)
}

func (s *OperandContext) SelectorChain() ISelectorChainContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ISelectorChainContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ISelectorChainContext)
}

func (s *OperandContext) Expression() IExpressionContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *OperandContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *OperandContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *OperandContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitOperand(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Operand() (localctx IOperandContext) {
	localctx = NewOperandContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 34, GoGenTemplateParserRULE_operand)
	p.SetState(205)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 14, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(199)
			p.Literal()
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(200)
			p.SelectorChain()
		}

	case 3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(201)
			p.Match(GoGenTemplateParserT__23)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(202)
			p.expression(0)
		}
		{
			p.SetState(203)
			p.Match(GoGenTemplateParserT__24)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	case antlr.ATNInvalidAltNumber:
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IUnaryExpressionContext is an interface to support dynamic dispatch.
type IUnaryExpressionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	NOT() antlr.TerminalNode
	Expression() IExpressionContext
	EXISTS() antlr.TerminalNode
	Variable() IVariableContext

	// IsUnaryExpressionContext differentiates from other interfaces.
	IsUnaryExpressionContext()
}

type UnaryExpressionContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyUnaryExpressionContext() *UnaryExpressionContext {
	var p = new(UnaryExpressionContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_unaryExpression
	return p
}

func InitEmptyUnaryExpressionContext(p *UnaryExpressionContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_unaryExpression
}

func (*UnaryExpressionContext) IsUnaryExpressionContext() {}

func NewUnaryExpressionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *UnaryExpressionContext {
	var p = new(UnaryExpressionContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_unaryExpression

	return p
}

func (s *UnaryExpressionContext) GetParser() antlr.Parser { return s.parser }

func (s *UnaryExpressionContext) NOT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserNOT, 0)
}

func (s *UnaryExpressionContext) Expression() IExpressionContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *UnaryExpressionContext) EXISTS() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserEXISTS, 0)
}

func (s *UnaryExpressionContext) Variable() IVariableContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IVariableContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IVariableContext)
}

func (s *UnaryExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *UnaryExpressionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *UnaryExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitUnaryExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) UnaryExpression() (localctx IUnaryExpressionContext) {
	localctx = NewUnaryExpressionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 36, GoGenTemplateParserRULE_unaryExpression)
	p.SetState(211)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case GoGenTemplateParserNOT:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(207)
			p.Match(GoGenTemplateParserNOT)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(208)
			p.expression(0)
		}

	case GoGenTemplateParserEXISTS:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(209)
			p.Match(GoGenTemplateParserEXISTS)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(210)
			p.Variable()
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ILiteralContext is an interface to support dynamic dispatch.
type ILiteralContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	BoolLiteral() IBoolLiteralContext
	IntLiteral() IIntLiteralContext
	FloatLiteral() IFloatLiteralContext
	StringLiteral() IStringLiteralContext

	// IsLiteralContext differentiates from other interfaces.
	IsLiteralContext()
}

type LiteralContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyLiteralContext() *LiteralContext {
	var p = new(LiteralContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_literal
	return p
}

func InitEmptyLiteralContext(p *LiteralContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_literal
}

func (*LiteralContext) IsLiteralContext() {}

func NewLiteralContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *LiteralContext {
	var p = new(LiteralContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_literal

	return p
}

func (s *LiteralContext) GetParser() antlr.Parser { return s.parser }

func (s *LiteralContext) BoolLiteral() IBoolLiteralContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IBoolLiteralContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IBoolLiteralContext)
}

func (s *LiteralContext) IntLiteral() IIntLiteralContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIntLiteralContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIntLiteralContext)
}

func (s *LiteralContext) FloatLiteral() IFloatLiteralContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IFloatLiteralContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IFloatLiteralContext)
}

func (s *LiteralContext) StringLiteral() IStringLiteralContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IStringLiteralContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IStringLiteralContext)
}

func (s *LiteralContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LiteralContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *LiteralContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitLiteral(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Literal() (localctx ILiteralContext) {
	localctx = NewLiteralContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 38, GoGenTemplateParserRULE_literal)
	p.SetState(217)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 16, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(213)
			p.BoolLiteral()
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(214)
			p.IntLiteral()
		}

	case 3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(215)
			p.FloatLiteral()
		}

	case 4:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(216)
			p.StringLiteral()
		}

	case antlr.ATNInvalidAltNumber:
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IBoolLiteralContext is an interface to support dynamic dispatch.
type IBoolLiteralContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsBoolLiteralContext differentiates from other interfaces.
	IsBoolLiteralContext()
}

type BoolLiteralContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBoolLiteralContext() *BoolLiteralContext {
	var p = new(BoolLiteralContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_boolLiteral
	return p
}

func InitEmptyBoolLiteralContext(p *BoolLiteralContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_boolLiteral
}

func (*BoolLiteralContext) IsBoolLiteralContext() {}

func NewBoolLiteralContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BoolLiteralContext {
	var p = new(BoolLiteralContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_boolLiteral

	return p
}

func (s *BoolLiteralContext) GetParser() antlr.Parser { return s.parser }
func (s *BoolLiteralContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BoolLiteralContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BoolLiteralContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitBoolLiteral(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) BoolLiteral() (localctx IBoolLiteralContext) {
	localctx = NewBoolLiteralContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 40, GoGenTemplateParserRULE_boolLiteral)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(219)
		_la = p.GetTokenStream().LA(1)

		if !(_la == GoGenTemplateParserT__25 || _la == GoGenTemplateParserT__26) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IIntLiteralContext is an interface to support dynamic dispatch.
type IIntLiteralContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	INT() antlr.TerminalNode

	// IsIntLiteralContext differentiates from other interfaces.
	IsIntLiteralContext()
}

type IntLiteralContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIntLiteralContext() *IntLiteralContext {
	var p = new(IntLiteralContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_intLiteral
	return p
}

func InitEmptyIntLiteralContext(p *IntLiteralContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_intLiteral
}

func (*IntLiteralContext) IsIntLiteralContext() {}

func NewIntLiteralContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IntLiteralContext {
	var p = new(IntLiteralContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_intLiteral

	return p
}

func (s *IntLiteralContext) GetParser() antlr.Parser { return s.parser }

func (s *IntLiteralContext) INT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserINT, 0)
}

func (s *IntLiteralContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IntLiteralContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IntLiteralContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitIntLiteral(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) IntLiteral() (localctx IIntLiteralContext) {
	localctx = NewIntLiteralContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 42, GoGenTemplateParserRULE_intLiteral)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(222)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == GoGenTemplateParserT__13 {
		{
			p.SetState(221)
			p.Match(GoGenTemplateParserT__13)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	}
	{
		p.SetState(224)
		p.Match(GoGenTemplateParserINT)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IFloatLiteralContext is an interface to support dynamic dispatch.
type IFloatLiteralContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	AllINT() []antlr.TerminalNode
	INT(i int) antlr.TerminalNode
	DOT() antlr.TerminalNode

	// IsFloatLiteralContext differentiates from other interfaces.
	IsFloatLiteralContext()
}

type FloatLiteralContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFloatLiteralContext() *FloatLiteralContext {
	var p = new(FloatLiteralContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_floatLiteral
	return p
}

func InitEmptyFloatLiteralContext(p *FloatLiteralContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_floatLiteral
}

func (*FloatLiteralContext) IsFloatLiteralContext() {}

func NewFloatLiteralContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FloatLiteralContext {
	var p = new(FloatLiteralContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_floatLiteral

	return p
}

func (s *FloatLiteralContext) GetParser() antlr.Parser { return s.parser }

func (s *FloatLiteralContext) AllINT() []antlr.TerminalNode {
	return s.GetTokens(GoGenTemplateParserINT)
}

func (s *FloatLiteralContext) INT(i int) antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserINT, i)
}

func (s *FloatLiteralContext) DOT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserDOT, 0)
}

func (s *FloatLiteralContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FloatLiteralContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FloatLiteralContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitFloatLiteral(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) FloatLiteral() (localctx IFloatLiteralContext) {
	localctx = NewFloatLiteralContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 44, GoGenTemplateParserRULE_floatLiteral)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(227)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == GoGenTemplateParserT__13 {
		{
			p.SetState(226)
			p.Match(GoGenTemplateParserT__13)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	}
	{
		p.SetState(229)
		p.Match(GoGenTemplateParserINT)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(230)
		p.Match(GoGenTemplateParserDOT)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(231)
		p.Match(GoGenTemplateParserINT)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IStringLiteralContext is an interface to support dynamic dispatch.
type IStringLiteralContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	STRING() antlr.TerminalNode

	// IsStringLiteralContext differentiates from other interfaces.
	IsStringLiteralContext()
}

type StringLiteralContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStringLiteralContext() *StringLiteralContext {
	var p = new(StringLiteralContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_stringLiteral
	return p
}

func InitEmptyStringLiteralContext(p *StringLiteralContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_stringLiteral
}

func (*StringLiteralContext) IsStringLiteralContext() {}

func NewStringLiteralContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StringLiteralContext {
	var p = new(StringLiteralContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_stringLiteral

	return p
}

func (s *StringLiteralContext) GetParser() antlr.Parser { return s.parser }

func (s *StringLiteralContext) STRING() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserSTRING, 0)
}

func (s *StringLiteralContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StringLiteralContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StringLiteralContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitStringLiteral(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) StringLiteral() (localctx IStringLiteralContext) {
	localctx = NewStringLiteralContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 46, GoGenTemplateParserRULE_stringLiteral)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(233)
		p.Match(GoGenTemplateParserSTRING)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ISelectorChainContext is an interface to support dynamic dispatch.
type ISelectorChainContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	RootSelector() IRootSelectorContext
	Variable() IVariableContext
	AllSelector() []ISelectorContext
	Selector(i int) ISelectorContext

	// IsSelectorChainContext differentiates from other interfaces.
	IsSelectorChainContext()
}

type SelectorChainContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySelectorChainContext() *SelectorChainContext {
	var p = new(SelectorChainContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_selectorChain
	return p
}

func InitEmptySelectorChainContext(p *SelectorChainContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_selectorChain
}

func (*SelectorChainContext) IsSelectorChainContext() {}

func NewSelectorChainContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SelectorChainContext {
	var p = new(SelectorChainContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_selectorChain

	return p
}

func (s *SelectorChainContext) GetParser() antlr.Parser { return s.parser }

func (s *SelectorChainContext) RootSelector() IRootSelectorContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IRootSelectorContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IRootSelectorContext)
}

func (s *SelectorChainContext) Variable() IVariableContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IVariableContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IVariableContext)
}

func (s *SelectorChainContext) AllSelector() []ISelectorContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(ISelectorContext); ok {
			len++
		}
	}

	tst := make([]ISelectorContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(ISelectorContext); ok {
			tst[i] = t.(ISelectorContext)
			i++
		}
	}

	return tst
}

func (s *SelectorChainContext) Selector(i int) ISelectorContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ISelectorContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(ISelectorContext)
}

func (s *SelectorChainContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SelectorChainContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SelectorChainContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitSelectorChain(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) SelectorChain() (localctx ISelectorChainContext) {
	localctx = NewSelectorChainContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 48, GoGenTemplateParserRULE_selectorChain)
	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(250)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 22, p.GetParserRuleContext()) {
	case 1:
		p.SetState(247)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}

		switch p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 21, p.GetParserRuleContext()) {
		case 1:
			{
				p.SetState(235)
				p.Variable()
			}

		case 2:
			{
				p.SetState(236)
				p.Variable()
			}
			p.SetState(238)
			p.GetErrorHandler().Sync(p)
			if p.HasError() {
				goto errorExit
			}
			_alt = 1
			for ok := true; ok; ok = _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
				switch _alt {
				case 1:
					{
						p.SetState(237)
						p.Selector()
					}

				default:
					p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
					goto errorExit
				}

				p.SetState(240)
				p.GetErrorHandler().Sync(p)
				_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 19, p.GetParserRuleContext())
				if p.HasError() {
					goto errorExit
				}
			}

		case 3:
			p.SetState(243)
			p.GetErrorHandler().Sync(p)
			if p.HasError() {
				goto errorExit
			}
			_alt = 1
			for ok := true; ok; ok = _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
				switch _alt {
				case 1:
					{
						p.SetState(242)
						p.Selector()
					}

				default:
					p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
					goto errorExit
				}

				p.SetState(245)
				p.GetErrorHandler().Sync(p)
				_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 20, p.GetParserRuleContext())
				if p.HasError() {
					goto errorExit
				}
			}

		case antlr.ATNInvalidAltNumber:
			goto errorExit
		}

	case 2:
		{
			p.SetState(249)
			p.RootSelector()
		}

	case antlr.ATNInvalidAltNumber:
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IRootSelectorContext is an interface to support dynamic dispatch.
type IRootSelectorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	DOT() antlr.TerminalNode
	Index() IIndexContext
	SelectorChain() ISelectorChainContext

	// IsRootSelectorContext differentiates from other interfaces.
	IsRootSelectorContext()
}

type RootSelectorContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyRootSelectorContext() *RootSelectorContext {
	var p = new(RootSelectorContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_rootSelector
	return p
}

func InitEmptyRootSelectorContext(p *RootSelectorContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_rootSelector
}

func (*RootSelectorContext) IsRootSelectorContext() {}

func NewRootSelectorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RootSelectorContext {
	var p = new(RootSelectorContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_rootSelector

	return p
}

func (s *RootSelectorContext) GetParser() antlr.Parser { return s.parser }

func (s *RootSelectorContext) DOT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserDOT, 0)
}

func (s *RootSelectorContext) Index() IIndexContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIndexContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIndexContext)
}

func (s *RootSelectorContext) SelectorChain() ISelectorChainContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ISelectorChainContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ISelectorChainContext)
}

func (s *RootSelectorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RootSelectorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RootSelectorContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitRootSelector(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) RootSelector() (localctx IRootSelectorContext) {
	localctx = NewRootSelectorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 50, GoGenTemplateParserRULE_rootSelector)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(252)
		p.Match(GoGenTemplateParserDOT)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(257)
	p.GetErrorHandler().Sync(p)

	if p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 24, p.GetParserRuleContext()) == 1 {
		{
			p.SetState(253)
			p.Index()
		}
		p.SetState(255)
		p.GetErrorHandler().Sync(p)

		if p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 23, p.GetParserRuleContext()) == 1 {
			{
				p.SetState(254)
				p.SelectorChain()
			}

		} else if p.HasError() { // JIM
			goto errorExit
		}

	} else if p.HasError() { // JIM
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ISelectorContext is an interface to support dynamic dispatch.
type ISelectorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	FieldSelector() IFieldSelectorContext
	Index() IIndexContext
	Slice() ISliceContext
	Call() ICallContext

	// IsSelectorContext differentiates from other interfaces.
	IsSelectorContext()
}

type SelectorContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySelectorContext() *SelectorContext {
	var p = new(SelectorContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_selector
	return p
}

func InitEmptySelectorContext(p *SelectorContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_selector
}

func (*SelectorContext) IsSelectorContext() {}

func NewSelectorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SelectorContext {
	var p = new(SelectorContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_selector

	return p
}

func (s *SelectorContext) GetParser() antlr.Parser { return s.parser }

func (s *SelectorContext) FieldSelector() IFieldSelectorContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IFieldSelectorContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IFieldSelectorContext)
}

func (s *SelectorContext) Index() IIndexContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIndexContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIndexContext)
}

func (s *SelectorContext) Slice() ISliceContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ISliceContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ISliceContext)
}

func (s *SelectorContext) Call() ICallContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ICallContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ICallContext)
}

func (s *SelectorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SelectorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SelectorContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitSelector(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Selector() (localctx ISelectorContext) {
	localctx = NewSelectorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 52, GoGenTemplateParserRULE_selector)
	p.SetState(263)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 25, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(259)
			p.FieldSelector()
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(260)
			p.Index()
		}

	case 3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(261)
			p.Slice()
		}

	case 4:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(262)
			p.Call()
		}

	case antlr.ATNInvalidAltNumber:
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IFieldSelectorContext is an interface to support dynamic dispatch.
type IFieldSelectorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	DOT() antlr.TerminalNode
	Identifier() IIdentifierContext

	// IsFieldSelectorContext differentiates from other interfaces.
	IsFieldSelectorContext()
}

type FieldSelectorContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFieldSelectorContext() *FieldSelectorContext {
	var p = new(FieldSelectorContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_fieldSelector
	return p
}

func InitEmptyFieldSelectorContext(p *FieldSelectorContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_fieldSelector
}

func (*FieldSelectorContext) IsFieldSelectorContext() {}

func NewFieldSelectorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FieldSelectorContext {
	var p = new(FieldSelectorContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_fieldSelector

	return p
}

func (s *FieldSelectorContext) GetParser() antlr.Parser { return s.parser }

func (s *FieldSelectorContext) DOT() antlr.TerminalNode {
	return s.GetToken(GoGenTemplateParserDOT, 0)
}

func (s *FieldSelectorContext) Identifier() IIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *FieldSelectorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FieldSelectorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FieldSelectorContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitFieldSelector(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) FieldSelector() (localctx IFieldSelectorContext) {
	localctx = NewFieldSelectorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 54, GoGenTemplateParserRULE_fieldSelector)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(265)
		p.Match(GoGenTemplateParserDOT)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(266)
		p.Identifier()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IIndexContext is an interface to support dynamic dispatch.
type IIndexContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Expression() IExpressionContext

	// IsIndexContext differentiates from other interfaces.
	IsIndexContext()
}

type IndexContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIndexContext() *IndexContext {
	var p = new(IndexContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_index
	return p
}

func InitEmptyIndexContext(p *IndexContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_index
}

func (*IndexContext) IsIndexContext() {}

func NewIndexContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IndexContext {
	var p = new(IndexContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_index

	return p
}

func (s *IndexContext) GetParser() antlr.Parser { return s.parser }

func (s *IndexContext) Expression() IExpressionContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *IndexContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IndexContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IndexContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitIndex(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Index() (localctx IIndexContext) {
	localctx = NewIndexContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 56, GoGenTemplateParserRULE_index)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(268)
		p.Match(GoGenTemplateParserT__27)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(269)
		p.expression(0)
	}
	{
		p.SetState(270)
		p.Match(GoGenTemplateParserT__28)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ISliceContext is an interface to support dynamic dispatch.
type ISliceContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetFrom returns the from rule contexts.
	GetFrom() IExpressionContext

	// GetTo returns the to rule contexts.
	GetTo() IExpressionContext

	// SetFrom sets the from rule contexts.
	SetFrom(IExpressionContext)

	// SetTo sets the to rule contexts.
	SetTo(IExpressionContext)

	// Getter signatures
	AllExpression() []IExpressionContext
	Expression(i int) IExpressionContext

	// IsSliceContext differentiates from other interfaces.
	IsSliceContext()
}

type SliceContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
	from   IExpressionContext
	to     IExpressionContext
}

func NewEmptySliceContext() *SliceContext {
	var p = new(SliceContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_slice
	return p
}

func InitEmptySliceContext(p *SliceContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_slice
}

func (*SliceContext) IsSliceContext() {}

func NewSliceContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SliceContext {
	var p = new(SliceContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_slice

	return p
}

func (s *SliceContext) GetParser() antlr.Parser { return s.parser }

func (s *SliceContext) GetFrom() IExpressionContext { return s.from }

func (s *SliceContext) GetTo() IExpressionContext { return s.to }

func (s *SliceContext) SetFrom(v IExpressionContext) { s.from = v }

func (s *SliceContext) SetTo(v IExpressionContext) { s.to = v }

func (s *SliceContext) AllExpression() []IExpressionContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExpressionContext); ok {
			len++
		}
	}

	tst := make([]IExpressionContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExpressionContext); ok {
			tst[i] = t.(IExpressionContext)
			i++
		}
	}

	return tst
}

func (s *SliceContext) Expression(i int) IExpressionContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *SliceContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SliceContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SliceContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitSlice(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Slice() (localctx ISliceContext) {
	localctx = NewSliceContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 58, GoGenTemplateParserRULE_slice)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(272)
		p.Match(GoGenTemplateParserT__27)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(274)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&35183784902656) != 0 {
		{
			p.SetState(273)

			var _x = p.expression(0)

			localctx.(*SliceContext).from = _x
		}

	}
	{
		p.SetState(276)
		p.Match(GoGenTemplateParserT__7)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(278)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&35183784902656) != 0 {
		{
			p.SetState(277)

			var _x = p.expression(0)

			localctx.(*SliceContext).to = _x
		}

	}
	{
		p.SetState(280)
		p.Match(GoGenTemplateParserT__28)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ICallContext is an interface to support dynamic dispatch.
type ICallContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsCallContext differentiates from other interfaces.
	IsCallContext()
}

type CallContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyCallContext() *CallContext {
	var p = new(CallContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_call
	return p
}

func InitEmptyCallContext(p *CallContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_call
}

func (*CallContext) IsCallContext() {}

func NewCallContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *CallContext {
	var p = new(CallContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_call

	return p
}

func (s *CallContext) GetParser() antlr.Parser { return s.parser }
func (s *CallContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CallContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *CallContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitCall(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Call() (localctx ICallContext) {
	localctx = NewCallContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 60, GoGenTemplateParserRULE_call)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(282)
		p.Match(GoGenTemplateParserT__23)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(283)
		p.Match(GoGenTemplateParserT__24)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IVariableContext is an interface to support dynamic dispatch.
type IVariableContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Identifier() IIdentifierContext

	// IsVariableContext differentiates from other interfaces.
	IsVariableContext()
}

type VariableContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyVariableContext() *VariableContext {
	var p = new(VariableContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_variable
	return p
}

func InitEmptyVariableContext(p *VariableContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = GoGenTemplateParserRULE_variable
}

func (*VariableContext) IsVariableContext() {}

func NewVariableContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *VariableContext {
	var p = new(VariableContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = GoGenTemplateParserRULE_variable

	return p
}

func (s *VariableContext) GetParser() antlr.Parser { return s.parser }

func (s *VariableContext) Identifier() IIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *VariableContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *VariableContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *VariableContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case GoGenTemplateVisitor:
		return t.VisitVariable(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *GoGenTemplateParser) Variable() (localctx IVariableContext) {
	localctx = NewVariableContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 62, GoGenTemplateParserRULE_variable)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(285)
		p.Identifier()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

func (p *GoGenTemplateParser) Sempred(localctx antlr.RuleContext, ruleIndex, predIndex int) bool {
	switch ruleIndex {
	case 16:
		var t *ExpressionContext = nil
		if localctx != nil {
			t = localctx.(*ExpressionContext)
		}
		return p.Expression_Sempred(t, predIndex)

	default:
		panic("No predicate with index: " + fmt.Sprint(ruleIndex))
	}
}

func (p *GoGenTemplateParser) Expression_Sempred(localctx antlr.RuleContext, predIndex int) bool {
	switch predIndex {
	case 0:
		return p.Precpred(p.GetParserRuleContext(), 6)

	case 1:
		return p.Precpred(p.GetParserRuleContext(), 5)

	case 2:
		return p.Precpred(p.GetParserRuleContext(), 4)

	case 3:
		return p.Precpred(p.GetParserRuleContext(), 3)

	case 4:
		return p.Precpred(p.GetParserRuleContext(), 2)

	case 5:
		return p.Precpred(p.GetParserRuleContext(), 1)

	default:
		panic("No predicate with index: " + fmt.Sprint(predIndex))
	}
}
