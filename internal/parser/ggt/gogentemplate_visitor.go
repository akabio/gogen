// Code generated from ./GoGenTemplate.g4 by ANTLR 4.13.0. DO NOT EDIT.

package ggt // GoGenTemplate
import "github.com/antlr4-go/antlr/v4"

// A complete Visitor for a parse tree produced by GoGenTemplateParser.
type GoGenTemplateVisitor interface {
	antlr.ParseTreeVisitor

	// Visit a parse tree produced by GoGenTemplateParser#identifier.
	VisitIdentifier(ctx *IdentifierContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#opn.
	VisitOpn(ctx *OpnContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#cls.
	VisitCls(ctx *ClsContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#end.
	VisitEnd(ctx *EndContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#forBlock.
	VisitForBlock(ctx *ForBlockContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#ifBlock.
	VisitIfBlock(ctx *IfBlockContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#elseBlock.
	VisitElseBlock(ctx *ElseBlockContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#indentBlock.
	VisitIndentBlock(ctx *IndentBlockContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#echo.
	VisitEcho(ctx *EchoContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#view.
	VisitView(ctx *ViewContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#viewParam.
	VisitViewParam(ctx *ViewParamContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#visitDefaultParameters.
	VisitVisitDefaultParameters(ctx *VisitDefaultParametersContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#defaultParam.
	VisitDefaultParam(ctx *DefaultParamContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#importParameters.
	VisitImportParameters(ctx *ImportParametersContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#importParameter.
	VisitImportParameter(ctx *ImportParameterContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#rootExpression.
	VisitRootExpression(ctx *RootExpressionContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#expression.
	VisitExpression(ctx *ExpressionContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#operand.
	VisitOperand(ctx *OperandContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#unaryExpression.
	VisitUnaryExpression(ctx *UnaryExpressionContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#literal.
	VisitLiteral(ctx *LiteralContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#boolLiteral.
	VisitBoolLiteral(ctx *BoolLiteralContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#intLiteral.
	VisitIntLiteral(ctx *IntLiteralContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#floatLiteral.
	VisitFloatLiteral(ctx *FloatLiteralContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#stringLiteral.
	VisitStringLiteral(ctx *StringLiteralContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#selectorChain.
	VisitSelectorChain(ctx *SelectorChainContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#rootSelector.
	VisitRootSelector(ctx *RootSelectorContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#selector.
	VisitSelector(ctx *SelectorContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#fieldSelector.
	VisitFieldSelector(ctx *FieldSelectorContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#index.
	VisitIndex(ctx *IndexContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#slice.
	VisitSlice(ctx *SliceContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#call.
	VisitCall(ctx *CallContext) interface{}

	// Visit a parse tree produced by GoGenTemplateParser#variable.
	VisitVariable(ctx *VariableContext) interface{}
}
