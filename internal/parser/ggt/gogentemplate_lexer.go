// Code generated from ./GoGenTemplate.g4 by ANTLR 4.13.0. DO NOT EDIT.

package ggt

import (
	"fmt"
	"github.com/antlr4-go/antlr/v4"
	"sync"
	"unicode"
)

// Suppress unused import error
var _ = fmt.Printf
var _ = sync.Once{}
var _ = unicode.IsLetter

type GoGenTemplateLexer struct {
	*antlr.BaseLexer
	channelNames []string
	modeNames    []string
	// TODO: EOF string
}

var GoGenTemplateLexerLexerStaticData struct {
	once                   sync.Once
	serializedATN          []int32
	ChannelNames           []string
	ModeNames              []string
	LiteralNames           []string
	SymbolicNames          []string
	RuleNames              []string
	PredictionContextCache *antlr.PredictionContextCache
	atn                    *antlr.ATN
	decisionToDFA          []*antlr.DFA
}

func gogentemplatelexerLexerInit() {
	staticData := &GoGenTemplateLexerLexerStaticData
	staticData.ChannelNames = []string{
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN",
	}
	staticData.ModeNames = []string{
		"DEFAULT_MODE",
	}
	staticData.LiteralNames = []string{
		"", "'{-'", "'{'", "'-}'", "'}'", "','", "'$'", "'@'", "':'", "'='",
		"'*'", "'/'", "'%'", "'+'", "'-'", "'=='", "'!='", "'<'", "'<='", "'>'",
		"'>='", "'&&'", "'||'", "'|'", "'('", "')'", "'true'", "'false'", "'['",
		"']'", "'for'", "'in'", "'end'", "'if'", "'indent'", "'else'", "'first'",
		"'last'", "'even'", "", "", "", "'.'", "'!'", "'?'",
	}
	staticData.SymbolicNames = []string{
		"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
		"", "", "", "", "", "", "", "", "", "", "", "", "", "FOR", "IN", "END",
		"IF", "INDENT", "ELSE", "FIRST", "LAST", "EVEN", "STRING", "IDENTIFIER",
		"INT", "DOT", "NOT", "EXISTS", "WS",
	}
	staticData.RuleNames = []string{
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8",
		"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16",
		"T__17", "T__18", "T__19", "T__20", "T__21", "T__22", "T__23", "T__24",
		"T__25", "T__26", "T__27", "T__28", "FOR", "IN", "END", "IF", "INDENT",
		"ELSE", "FIRST", "LAST", "EVEN", "STRING", "IDENTIFIER", "INT", "DOT",
		"NOT", "EXISTS", "WS",
	}
	staticData.PredictionContextCache = antlr.NewPredictionContextCache()
	staticData.serializedATN = []int32{
		4, 0, 45, 240, 6, -1, 2, 0, 7, 0, 2, 1, 7, 1, 2, 2, 7, 2, 2, 3, 7, 3, 2,
		4, 7, 4, 2, 5, 7, 5, 2, 6, 7, 6, 2, 7, 7, 7, 2, 8, 7, 8, 2, 9, 7, 9, 2,
		10, 7, 10, 2, 11, 7, 11, 2, 12, 7, 12, 2, 13, 7, 13, 2, 14, 7, 14, 2, 15,
		7, 15, 2, 16, 7, 16, 2, 17, 7, 17, 2, 18, 7, 18, 2, 19, 7, 19, 2, 20, 7,
		20, 2, 21, 7, 21, 2, 22, 7, 22, 2, 23, 7, 23, 2, 24, 7, 24, 2, 25, 7, 25,
		2, 26, 7, 26, 2, 27, 7, 27, 2, 28, 7, 28, 2, 29, 7, 29, 2, 30, 7, 30, 2,
		31, 7, 31, 2, 32, 7, 32, 2, 33, 7, 33, 2, 34, 7, 34, 2, 35, 7, 35, 2, 36,
		7, 36, 2, 37, 7, 37, 2, 38, 7, 38, 2, 39, 7, 39, 2, 40, 7, 40, 2, 41, 7,
		41, 2, 42, 7, 42, 2, 43, 7, 43, 2, 44, 7, 44, 1, 0, 1, 0, 1, 0, 1, 1, 1,
		1, 1, 2, 1, 2, 1, 2, 1, 3, 1, 3, 1, 4, 1, 4, 1, 5, 1, 5, 1, 6, 1, 6, 1,
		7, 1, 7, 1, 8, 1, 8, 1, 9, 1, 9, 1, 10, 1, 10, 1, 11, 1, 11, 1, 12, 1,
		12, 1, 13, 1, 13, 1, 14, 1, 14, 1, 14, 1, 15, 1, 15, 1, 15, 1, 16, 1, 16,
		1, 17, 1, 17, 1, 17, 1, 18, 1, 18, 1, 19, 1, 19, 1, 19, 1, 20, 1, 20, 1,
		20, 1, 21, 1, 21, 1, 21, 1, 22, 1, 22, 1, 23, 1, 23, 1, 24, 1, 24, 1, 25,
		1, 25, 1, 25, 1, 25, 1, 25, 1, 26, 1, 26, 1, 26, 1, 26, 1, 26, 1, 26, 1,
		27, 1, 27, 1, 28, 1, 28, 1, 29, 1, 29, 1, 29, 1, 29, 1, 30, 1, 30, 1, 30,
		1, 31, 1, 31, 1, 31, 1, 31, 1, 32, 1, 32, 1, 32, 1, 33, 1, 33, 1, 33, 1,
		33, 1, 33, 1, 33, 1, 33, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 35, 1, 35,
		1, 35, 1, 35, 1, 35, 1, 35, 1, 36, 1, 36, 1, 36, 1, 36, 1, 36, 1, 37, 1,
		37, 1, 37, 1, 37, 1, 37, 1, 38, 1, 38, 5, 38, 209, 8, 38, 10, 38, 12, 38,
		212, 9, 38, 1, 38, 1, 38, 1, 39, 1, 39, 5, 39, 218, 8, 39, 10, 39, 12,
		39, 221, 9, 39, 1, 40, 4, 40, 224, 8, 40, 11, 40, 12, 40, 225, 1, 41, 1,
		41, 1, 42, 1, 42, 1, 43, 1, 43, 1, 44, 4, 44, 235, 8, 44, 11, 44, 12, 44,
		236, 1, 44, 1, 44, 1, 210, 0, 45, 1, 1, 3, 2, 5, 3, 7, 4, 9, 5, 11, 6,
		13, 7, 15, 8, 17, 9, 19, 10, 21, 11, 23, 12, 25, 13, 27, 14, 29, 15, 31,
		16, 33, 17, 35, 18, 37, 19, 39, 20, 41, 21, 43, 22, 45, 23, 47, 24, 49,
		25, 51, 26, 53, 27, 55, 28, 57, 29, 59, 30, 61, 31, 63, 32, 65, 33, 67,
		34, 69, 35, 71, 36, 73, 37, 75, 38, 77, 39, 79, 40, 81, 41, 83, 42, 85,
		43, 87, 44, 89, 45, 1, 0, 4, 2, 0, 65, 90, 97, 122, 3, 0, 48, 57, 65, 90,
		97, 122, 1, 0, 48, 57, 2, 0, 9, 9, 32, 32, 243, 0, 1, 1, 0, 0, 0, 0, 3,
		1, 0, 0, 0, 0, 5, 1, 0, 0, 0, 0, 7, 1, 0, 0, 0, 0, 9, 1, 0, 0, 0, 0, 11,
		1, 0, 0, 0, 0, 13, 1, 0, 0, 0, 0, 15, 1, 0, 0, 0, 0, 17, 1, 0, 0, 0, 0,
		19, 1, 0, 0, 0, 0, 21, 1, 0, 0, 0, 0, 23, 1, 0, 0, 0, 0, 25, 1, 0, 0, 0,
		0, 27, 1, 0, 0, 0, 0, 29, 1, 0, 0, 0, 0, 31, 1, 0, 0, 0, 0, 33, 1, 0, 0,
		0, 0, 35, 1, 0, 0, 0, 0, 37, 1, 0, 0, 0, 0, 39, 1, 0, 0, 0, 0, 41, 1, 0,
		0, 0, 0, 43, 1, 0, 0, 0, 0, 45, 1, 0, 0, 0, 0, 47, 1, 0, 0, 0, 0, 49, 1,
		0, 0, 0, 0, 51, 1, 0, 0, 0, 0, 53, 1, 0, 0, 0, 0, 55, 1, 0, 0, 0, 0, 57,
		1, 0, 0, 0, 0, 59, 1, 0, 0, 0, 0, 61, 1, 0, 0, 0, 0, 63, 1, 0, 0, 0, 0,
		65, 1, 0, 0, 0, 0, 67, 1, 0, 0, 0, 0, 69, 1, 0, 0, 0, 0, 71, 1, 0, 0, 0,
		0, 73, 1, 0, 0, 0, 0, 75, 1, 0, 0, 0, 0, 77, 1, 0, 0, 0, 0, 79, 1, 0, 0,
		0, 0, 81, 1, 0, 0, 0, 0, 83, 1, 0, 0, 0, 0, 85, 1, 0, 0, 0, 0, 87, 1, 0,
		0, 0, 0, 89, 1, 0, 0, 0, 1, 91, 1, 0, 0, 0, 3, 94, 1, 0, 0, 0, 5, 96, 1,
		0, 0, 0, 7, 99, 1, 0, 0, 0, 9, 101, 1, 0, 0, 0, 11, 103, 1, 0, 0, 0, 13,
		105, 1, 0, 0, 0, 15, 107, 1, 0, 0, 0, 17, 109, 1, 0, 0, 0, 19, 111, 1,
		0, 0, 0, 21, 113, 1, 0, 0, 0, 23, 115, 1, 0, 0, 0, 25, 117, 1, 0, 0, 0,
		27, 119, 1, 0, 0, 0, 29, 121, 1, 0, 0, 0, 31, 124, 1, 0, 0, 0, 33, 127,
		1, 0, 0, 0, 35, 129, 1, 0, 0, 0, 37, 132, 1, 0, 0, 0, 39, 134, 1, 0, 0,
		0, 41, 137, 1, 0, 0, 0, 43, 140, 1, 0, 0, 0, 45, 143, 1, 0, 0, 0, 47, 145,
		1, 0, 0, 0, 49, 147, 1, 0, 0, 0, 51, 149, 1, 0, 0, 0, 53, 154, 1, 0, 0,
		0, 55, 160, 1, 0, 0, 0, 57, 162, 1, 0, 0, 0, 59, 164, 1, 0, 0, 0, 61, 168,
		1, 0, 0, 0, 63, 171, 1, 0, 0, 0, 65, 175, 1, 0, 0, 0, 67, 178, 1, 0, 0,
		0, 69, 185, 1, 0, 0, 0, 71, 190, 1, 0, 0, 0, 73, 196, 1, 0, 0, 0, 75, 201,
		1, 0, 0, 0, 77, 206, 1, 0, 0, 0, 79, 215, 1, 0, 0, 0, 81, 223, 1, 0, 0,
		0, 83, 227, 1, 0, 0, 0, 85, 229, 1, 0, 0, 0, 87, 231, 1, 0, 0, 0, 89, 234,
		1, 0, 0, 0, 91, 92, 5, 123, 0, 0, 92, 93, 5, 45, 0, 0, 93, 2, 1, 0, 0,
		0, 94, 95, 5, 123, 0, 0, 95, 4, 1, 0, 0, 0, 96, 97, 5, 45, 0, 0, 97, 98,
		5, 125, 0, 0, 98, 6, 1, 0, 0, 0, 99, 100, 5, 125, 0, 0, 100, 8, 1, 0, 0,
		0, 101, 102, 5, 44, 0, 0, 102, 10, 1, 0, 0, 0, 103, 104, 5, 36, 0, 0, 104,
		12, 1, 0, 0, 0, 105, 106, 5, 64, 0, 0, 106, 14, 1, 0, 0, 0, 107, 108, 5,
		58, 0, 0, 108, 16, 1, 0, 0, 0, 109, 110, 5, 61, 0, 0, 110, 18, 1, 0, 0,
		0, 111, 112, 5, 42, 0, 0, 112, 20, 1, 0, 0, 0, 113, 114, 5, 47, 0, 0, 114,
		22, 1, 0, 0, 0, 115, 116, 5, 37, 0, 0, 116, 24, 1, 0, 0, 0, 117, 118, 5,
		43, 0, 0, 118, 26, 1, 0, 0, 0, 119, 120, 5, 45, 0, 0, 120, 28, 1, 0, 0,
		0, 121, 122, 5, 61, 0, 0, 122, 123, 5, 61, 0, 0, 123, 30, 1, 0, 0, 0, 124,
		125, 5, 33, 0, 0, 125, 126, 5, 61, 0, 0, 126, 32, 1, 0, 0, 0, 127, 128,
		5, 60, 0, 0, 128, 34, 1, 0, 0, 0, 129, 130, 5, 60, 0, 0, 130, 131, 5, 61,
		0, 0, 131, 36, 1, 0, 0, 0, 132, 133, 5, 62, 0, 0, 133, 38, 1, 0, 0, 0,
		134, 135, 5, 62, 0, 0, 135, 136, 5, 61, 0, 0, 136, 40, 1, 0, 0, 0, 137,
		138, 5, 38, 0, 0, 138, 139, 5, 38, 0, 0, 139, 42, 1, 0, 0, 0, 140, 141,
		5, 124, 0, 0, 141, 142, 5, 124, 0, 0, 142, 44, 1, 0, 0, 0, 143, 144, 5,
		124, 0, 0, 144, 46, 1, 0, 0, 0, 145, 146, 5, 40, 0, 0, 146, 48, 1, 0, 0,
		0, 147, 148, 5, 41, 0, 0, 148, 50, 1, 0, 0, 0, 149, 150, 5, 116, 0, 0,
		150, 151, 5, 114, 0, 0, 151, 152, 5, 117, 0, 0, 152, 153, 5, 101, 0, 0,
		153, 52, 1, 0, 0, 0, 154, 155, 5, 102, 0, 0, 155, 156, 5, 97, 0, 0, 156,
		157, 5, 108, 0, 0, 157, 158, 5, 115, 0, 0, 158, 159, 5, 101, 0, 0, 159,
		54, 1, 0, 0, 0, 160, 161, 5, 91, 0, 0, 161, 56, 1, 0, 0, 0, 162, 163, 5,
		93, 0, 0, 163, 58, 1, 0, 0, 0, 164, 165, 5, 102, 0, 0, 165, 166, 5, 111,
		0, 0, 166, 167, 5, 114, 0, 0, 167, 60, 1, 0, 0, 0, 168, 169, 5, 105, 0,
		0, 169, 170, 5, 110, 0, 0, 170, 62, 1, 0, 0, 0, 171, 172, 5, 101, 0, 0,
		172, 173, 5, 110, 0, 0, 173, 174, 5, 100, 0, 0, 174, 64, 1, 0, 0, 0, 175,
		176, 5, 105, 0, 0, 176, 177, 5, 102, 0, 0, 177, 66, 1, 0, 0, 0, 178, 179,
		5, 105, 0, 0, 179, 180, 5, 110, 0, 0, 180, 181, 5, 100, 0, 0, 181, 182,
		5, 101, 0, 0, 182, 183, 5, 110, 0, 0, 183, 184, 5, 116, 0, 0, 184, 68,
		1, 0, 0, 0, 185, 186, 5, 101, 0, 0, 186, 187, 5, 108, 0, 0, 187, 188, 5,
		115, 0, 0, 188, 189, 5, 101, 0, 0, 189, 70, 1, 0, 0, 0, 190, 191, 5, 102,
		0, 0, 191, 192, 5, 105, 0, 0, 192, 193, 5, 114, 0, 0, 193, 194, 5, 115,
		0, 0, 194, 195, 5, 116, 0, 0, 195, 72, 1, 0, 0, 0, 196, 197, 5, 108, 0,
		0, 197, 198, 5, 97, 0, 0, 198, 199, 5, 115, 0, 0, 199, 200, 5, 116, 0,
		0, 200, 74, 1, 0, 0, 0, 201, 202, 5, 101, 0, 0, 202, 203, 5, 118, 0, 0,
		203, 204, 5, 101, 0, 0, 204, 205, 5, 110, 0, 0, 205, 76, 1, 0, 0, 0, 206,
		210, 5, 34, 0, 0, 207, 209, 9, 0, 0, 0, 208, 207, 1, 0, 0, 0, 209, 212,
		1, 0, 0, 0, 210, 211, 1, 0, 0, 0, 210, 208, 1, 0, 0, 0, 211, 213, 1, 0,
		0, 0, 212, 210, 1, 0, 0, 0, 213, 214, 5, 34, 0, 0, 214, 78, 1, 0, 0, 0,
		215, 219, 7, 0, 0, 0, 216, 218, 7, 1, 0, 0, 217, 216, 1, 0, 0, 0, 218,
		221, 1, 0, 0, 0, 219, 217, 1, 0, 0, 0, 219, 220, 1, 0, 0, 0, 220, 80, 1,
		0, 0, 0, 221, 219, 1, 0, 0, 0, 222, 224, 7, 2, 0, 0, 223, 222, 1, 0, 0,
		0, 224, 225, 1, 0, 0, 0, 225, 223, 1, 0, 0, 0, 225, 226, 1, 0, 0, 0, 226,
		82, 1, 0, 0, 0, 227, 228, 5, 46, 0, 0, 228, 84, 1, 0, 0, 0, 229, 230, 5,
		33, 0, 0, 230, 86, 1, 0, 0, 0, 231, 232, 5, 63, 0, 0, 232, 88, 1, 0, 0,
		0, 233, 235, 7, 3, 0, 0, 234, 233, 1, 0, 0, 0, 235, 236, 1, 0, 0, 0, 236,
		234, 1, 0, 0, 0, 236, 237, 1, 0, 0, 0, 237, 238, 1, 0, 0, 0, 238, 239,
		6, 44, 0, 0, 239, 90, 1, 0, 0, 0, 5, 0, 210, 219, 225, 236, 1, 6, 0, 0,
	}
	deserializer := antlr.NewATNDeserializer(nil)
	staticData.atn = deserializer.Deserialize(staticData.serializedATN)
	atn := staticData.atn
	staticData.decisionToDFA = make([]*antlr.DFA, len(atn.DecisionToState))
	decisionToDFA := staticData.decisionToDFA
	for index, state := range atn.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(state, index)
	}
}

// GoGenTemplateLexerInit initializes any static state used to implement GoGenTemplateLexer. By default the
// static state used to implement the lexer is lazily initialized during the first call to
// NewGoGenTemplateLexer(). You can call this function if you wish to initialize the static state ahead
// of time.
func GoGenTemplateLexerInit() {
	staticData := &GoGenTemplateLexerLexerStaticData
	staticData.once.Do(gogentemplatelexerLexerInit)
}

// NewGoGenTemplateLexer produces a new lexer instance for the optional input antlr.CharStream.
func NewGoGenTemplateLexer(input antlr.CharStream) *GoGenTemplateLexer {
	GoGenTemplateLexerInit()
	l := new(GoGenTemplateLexer)
	l.BaseLexer = antlr.NewBaseLexer(input)
	staticData := &GoGenTemplateLexerLexerStaticData
	l.Interpreter = antlr.NewLexerATNSimulator(l, staticData.atn, staticData.decisionToDFA, staticData.PredictionContextCache)
	l.channelNames = staticData.ChannelNames
	l.modeNames = staticData.ModeNames
	l.RuleNames = staticData.RuleNames
	l.LiteralNames = staticData.LiteralNames
	l.SymbolicNames = staticData.SymbolicNames
	l.GrammarFileName = "GoGenTemplate.g4"
	// TODO: l.EOF = antlr.TokenEOF

	return l
}

// GoGenTemplateLexer tokens.
const (
	GoGenTemplateLexerT__0       = 1
	GoGenTemplateLexerT__1       = 2
	GoGenTemplateLexerT__2       = 3
	GoGenTemplateLexerT__3       = 4
	GoGenTemplateLexerT__4       = 5
	GoGenTemplateLexerT__5       = 6
	GoGenTemplateLexerT__6       = 7
	GoGenTemplateLexerT__7       = 8
	GoGenTemplateLexerT__8       = 9
	GoGenTemplateLexerT__9       = 10
	GoGenTemplateLexerT__10      = 11
	GoGenTemplateLexerT__11      = 12
	GoGenTemplateLexerT__12      = 13
	GoGenTemplateLexerT__13      = 14
	GoGenTemplateLexerT__14      = 15
	GoGenTemplateLexerT__15      = 16
	GoGenTemplateLexerT__16      = 17
	GoGenTemplateLexerT__17      = 18
	GoGenTemplateLexerT__18      = 19
	GoGenTemplateLexerT__19      = 20
	GoGenTemplateLexerT__20      = 21
	GoGenTemplateLexerT__21      = 22
	GoGenTemplateLexerT__22      = 23
	GoGenTemplateLexerT__23      = 24
	GoGenTemplateLexerT__24      = 25
	GoGenTemplateLexerT__25      = 26
	GoGenTemplateLexerT__26      = 27
	GoGenTemplateLexerT__27      = 28
	GoGenTemplateLexerT__28      = 29
	GoGenTemplateLexerFOR        = 30
	GoGenTemplateLexerIN         = 31
	GoGenTemplateLexerEND        = 32
	GoGenTemplateLexerIF         = 33
	GoGenTemplateLexerINDENT     = 34
	GoGenTemplateLexerELSE       = 35
	GoGenTemplateLexerFIRST      = 36
	GoGenTemplateLexerLAST       = 37
	GoGenTemplateLexerEVEN       = 38
	GoGenTemplateLexerSTRING     = 39
	GoGenTemplateLexerIDENTIFIER = 40
	GoGenTemplateLexerINT        = 41
	GoGenTemplateLexerDOT        = 42
	GoGenTemplateLexerNOT        = 43
	GoGenTemplateLexerEXISTS     = 44
	GoGenTemplateLexerWS         = 45
)
