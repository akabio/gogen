// Code generated from ./GoGenTemplate.g4 by ANTLR 4.13.0. DO NOT EDIT.

package ggt // GoGenTemplate
import "github.com/antlr4-go/antlr/v4"

type BaseGoGenTemplateVisitor struct {
	*antlr.BaseParseTreeVisitor
}

func (v *BaseGoGenTemplateVisitor) VisitIdentifier(ctx *IdentifierContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitOpn(ctx *OpnContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitCls(ctx *ClsContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitEnd(ctx *EndContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitForBlock(ctx *ForBlockContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitIfBlock(ctx *IfBlockContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitElseBlock(ctx *ElseBlockContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitIndentBlock(ctx *IndentBlockContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitEcho(ctx *EchoContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitView(ctx *ViewContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitViewParam(ctx *ViewParamContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitVisitDefaultParameters(ctx *VisitDefaultParametersContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitDefaultParam(ctx *DefaultParamContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitImportParameters(ctx *ImportParametersContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitImportParameter(ctx *ImportParameterContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitRootExpression(ctx *RootExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitExpression(ctx *ExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitOperand(ctx *OperandContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitUnaryExpression(ctx *UnaryExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitLiteral(ctx *LiteralContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitBoolLiteral(ctx *BoolLiteralContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitIntLiteral(ctx *IntLiteralContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitFloatLiteral(ctx *FloatLiteralContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitStringLiteral(ctx *StringLiteralContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitSelectorChain(ctx *SelectorChainContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitRootSelector(ctx *RootSelectorContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitSelector(ctx *SelectorContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitFieldSelector(ctx *FieldSelectorContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitIndex(ctx *IndexContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitSlice(ctx *SliceContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitCall(ctx *CallContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseGoGenTemplateVisitor) VisitVariable(ctx *VariableContext) interface{} {
	return v.VisitChildren(ctx)
}
