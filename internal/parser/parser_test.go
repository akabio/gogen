package parser_test

import (
	"testing"

	"gitlab.com/akabio/gogen/internal/parser"
)

func BenchmarkParser(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := parser.Parse(BENCH1, "bench1.gg", func(i string) (string, string, error) {
			return "", "", nil
		})
		if err != nil {
			b.Fatal(err)
		}
	}
}

const BENCH1 = `
main visitor:
foo.Bar type:
	We are now static text
	${.Name | upper}
	{for i, x  in .Slots}
		{if !done}
			{for i, x in .Slots}
				{if !done}
					${.Name | upper}
				{end}
			{end}
			{for i, x  in .Slots}
				{if !done}
					{for i, x  in .Slots}
						{if !done}
							{for i, x  in .Slots}
								{if !done}
									${.Name | upper}
								{end}
							{end}
							{for i, x  in .Slots}
								{if !done}

								{end}
							{end}
							{for i, x  in .Slots}
								{if !done}

								{end}
							{end}
						{end}
					{end}
				{end}
			{end}
			{for i, x in .Slots}
				{if !done}
					TROMPETE
				{end}
			{end}
		{end}
	{end}
`
