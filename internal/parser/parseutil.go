package parser

import (
	"strings"

	"github.com/antlr4-go/antlr/v4"
	"gitlab.com/akabio/gogen/ast"
	"gitlab.com/akabio/gogen/common"
	"gitlab.com/akabio/gogen/internal/parser/ggt"
)

func parseSilent(opn ggt.IOpnContext, cls ggt.IClsContext) ast.Silent {
	o := ""
	if opn != nil {
		o = opn.GetText()
	}

	c := ""
	if cls != nil {
		c = cls.GetText()
	}

	return ast.Silent{
		Before: strings.Contains(o, "-"),
		After:  strings.Contains(c, "-"),
	}
}

func (v *TemplateVisitor) createBaseNode(start antlr.Token) *ast.BaseNode {
	return &ast.BaseNode{
		Location: v.loc.Add(common.Loc("",
			start.GetLine()-1, // line is one based in antlr, so -1
			start.GetColumn(), // column is 0 based
		)),
	}
}

func (v *TemplateVisitor) createBaseNodeFromRule(prc antlr.ParserRuleContext) *ast.BaseNode {
	return v.createBaseNode(prc.GetStart())
}
