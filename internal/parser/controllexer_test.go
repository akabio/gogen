package parser

import (
	"fmt"
	"testing"

	"github.com/akabio/expect"
)

type lexTest struct {
	name      string
	source    string
	symbols   []symbolType
	strs      []string
	locations []int
}

var lexTests = []lexTest{
	{
		name:    "eof symbol",
		source:  "",
		symbols: []symbolType{EOF},
	},
	{
		name:    "text symbol",
		source:  "sdasd dsa dsa ds",
		symbols: []symbolType{Text, EOF},
	},
	{
		name:    "print symbol",
		source:  "${ foo bar baz}",
		symbols: []symbolType{Print, EOF},
		strs:    []string{"${ foo bar baz}"},
	},
	{
		name:    "visit symbol",
		source:  "@{ foo bar baz}",
		symbols: []symbolType{Visit, EOF},
	},
	{
		name:    "visit import",
		source:  "@{test.foo: bar baz}",
		symbols: []symbolType{Visit, EOF},
	},
	{
		name:    "end",
		source:  "{ end }",
		symbols: []symbolType{End, EOF},
	},
	{
		name:    "if",
		source:  "{ if foobar == 17 }",
		symbols: []symbolType{If, EOF},
		strs:    []string{"{ if foobar == 17 }"},
	},
	{
		name:    "silent if",
		source:  "{- if true }{ if true -}{- if true -}",
		symbols: []symbolType{If, If, If, EOF},
	},
	{
		name:    "if-end",
		source:  "{if .True}true{end}{if .False}false{end}",
		symbols: []symbolType{If, Text, End, If, Text, End, EOF},
		strs:    []string{"{if .True}", "true"},
	},
	{
		name:    "else",
		source:  "{ else }",
		symbols: []symbolType{Else, EOF},
	},
	{
		name:    "else if",
		source:  "{ else if }",
		symbols: []symbolType{ElseIf, EOF},
	},
	{
		name:    "indent",
		source:  "{indent 3}",
		symbols: []symbolType{Indent, EOF},
	},
	{
		name:    "for",
		source:  "{ for }",
		symbols: []symbolType{For, EOF},
		strs:    []string{"{ for }"},
	},
	{
		name:      "print and text mixed",
		source:    "Hello ${ foo bar baz} blabla ${6dfd}",
		symbols:   []symbolType{Text, Print, Text, Print, EOF},
		strs:      []string{"Hello ", "${ foo bar baz}", " blabla ", "${6dfd}"},
		locations: []int{0, 0, 0, 6, 0, 21, 0, 29},
	},
	{
		name:    "unclosed statemenet",
		source:  "Hello { end dfeds",
		symbols: []symbolType{Text, Text, EOF},
	},
}

func TestLexint(t *testing.T) {
	for _, tc := range lexTests {
		t.Run(tc.name, func(t *testing.T) {
			lexer := &controlLexer{source: []rune(tc.source)}
			sym := lexer.next()
			i := 0
			for {
				if i >= len(tc.symbols) {
					t.Fatalf("have more tokens than expected %v", len(tc.symbols))
				}
				expect.Value(t, fmt.Sprintf("symbol %v", i), sym.t).ToBe(tc.symbols[i])

				if i < len(tc.strs) {
					expect.Value(t, fmt.Sprintf("str %v", i), lexer.str(sym)).ToBe(tc.strs[i])
				}

				if i < len(tc.locations)/2 {
					line, column := lexer.position(sym)
					loc := fmt.Sprintf("%v:%v", line, column)
					expected := fmt.Sprintf("%v:%v", tc.locations[i*2], tc.locations[i*2+1])
					expect.Value(t, fmt.Sprintf("location %v", i), loc).ToBe(expected)
				}

				if sym.t == EOF {
					break
				}

				sym = lexer.next()
				i++
			}
		})
	}
}
