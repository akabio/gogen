package parser

import (
	"strings"

	"gitlab.com/akabio/gogen/ast"
	"gitlab.com/akabio/gogen/ggerr"
)

// Parse a gogen template
func Parse(code, source string, imports func(i string) (string, string, error)) (*ast.AST, error) {
	ast, errors := ParseDetailed(code, source, imports)
	if errors.Has() {
		return nil, errors
	}

	return ast, nil
}

// ParseDetailed will always return an error object which contains details about all found errors
func ParseDetailed(code, source string, imports func(i string) (string, string, error)) (*ast.AST, *ggerr.MultiError) {
	ast, errors := parseFile(code, source, imports)
	return reduceAST(ast), errors
}

func reduceAST(ast *ast.AST) *ast.AST {
	for _, v := range ast.Visitors {
		for _, t := range v.Templates {
			t.Nodes = reduceTextNodes(t.Nodes)
			// templates will always have silent ends
			removeSilentWhitespace(t.Nodes, false, true)
		}
	}

	return ast
}

// reduceTextNodes will merge sibling text nodes together and remove silent whitespace
func reduceTextNodes(ns []ast.Node) []ast.Node {
	if len(ns) == 0 {
		return ns
	}

	nns := []ast.Node{ns[0]}

	// merge text nodes together
	canMerge := func(n ast.Node) bool {
		_, istxt := n.(*ast.TextNode)
		if !istxt {
			return false
		}

		_, lastistxt := nns[len(nns)-1].(*ast.TextNode)

		return lastistxt
	}

	for _, n := range ns[1:] {
		if canMerge(n) {
			nns[len(nns)-1].(*ast.TextNode).Text += n.(*ast.TextNode).Text
		} else {
			nns = append(nns, n)
		}
	}

	// remove silent whitespace
	for i, n := range nns {
		before := false
		after := false

		switch t := n.(type) {
		case *ast.VisitNode:
			before = t.Silent.Before
			after = t.Silent.After
		case *ast.EchoNode:
			before = t.Silent.Before
			after = t.Silent.After
		case *ast.ForNode:
			removeSilentWhitespace(t.Nodes, t.SilentFor.After, t.SilentEnd.Before)
			before = t.SilentFor.Before
			after = t.SilentEnd.After
		case *ast.IndentNode:
			removeSilentWhitespace(t.Nodes, t.SilentIndent.After, t.SilentEnd.Before)
			before = t.SilentIndent.Before
			after = t.SilentEnd.After
		case *ast.IfNode:
			if t.ElseNodes == nil {
				removeSilentWhitespace(t.IfNodes, t.SilentIf.After, t.SilentEnd.Before)
			} else {
				removeSilentWhitespace(t.IfNodes, t.SilentIf.After, t.SilentElse.Before)
				removeSilentWhitespace(t.ElseNodes, t.SilentElse.After, t.SilentEnd.Before)
			}

			before = t.SilentIf.Before
			after = t.SilentEnd.After
		}

		if before && i > 0 {
			removeSilentWhitespace(nns[i-1:i], false, true)
		}

		if after && i < len(nns)-1 {
			removeSilentWhitespace(nns[i+1:i+2], true, false)
		}
	}

	// traverse down nodes with possible child text nodes...
	for _, n := range nns {
		switch t := n.(type) {
		case *ast.ForNode:
			t.Nodes = reduceTextNodes(t.Nodes)
		case *ast.IfNode:
			t.IfNodes = reduceTextNodes(t.IfNodes)
			t.ElseNodes = reduceTextNodes(t.ElseNodes)
		case *ast.IndentNode:
			t.Nodes = reduceTextNodes(t.Nodes)
		}
	}

	return nns
}

func removeSilentWhitespace(nodes []ast.Node, start, end bool) {
	if len(nodes) == 0 {
		return
	}

	if start {
		n, is := nodes[0].(*ast.TextNode)
		if is {
			n.Text = strings.TrimLeft(n.Text, " \t\r\n")
		}
	}

	if end {
		n, is := nodes[len(nodes)-1].(*ast.TextNode)
		if is {
			n.Text = strings.TrimRight(n.Text, " \t\r\n")
		}
	}
}
