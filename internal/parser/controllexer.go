package parser

type controlLexer struct {
	source []rune
	index  int
}

type symbolType string

const (
	Text   = symbolType("text")
	For    = symbolType("for")
	Else   = symbolType("else")
	ElseIf = symbolType("elseif")
	End    = symbolType("end")
	If     = symbolType("if")
	Print  = symbolType("print")
	Visit  = symbolType("visit")
	Indent = symbolType("indent")
	EOF    = symbolType("eof")
)

type symbol struct {
	t     symbolType
	start int
	end   int
}

func (l *controlLexer) position(s symbol) (int, int) {
	line := 0
	col := 0

	for i := 0; i < s.start; i++ {
		if l.source[i] == '\n' {
			line++

			col = 0
		} else {
			col++
		}
	}

	return line, col
}

func (l *controlLexer) remainingCount() int {
	return len(l.source) - l.index
}

func (l *controlLexer) str(s symbol) string {
	return string(l.source[s.start:s.end])
}

func (l *controlLexer) isAt(i int, s string) bool {
	rs := []rune(s)
	for c := 0; c < len(rs); c++ {
		sIndex := l.index + i + c
		if sIndex >= len(l.source) {
			return false
		}

		if l.source[sIndex] != rs[c] {
			return false
		}
	}

	return true
}

// skipSpace will skip space or -.
func (l *controlLexer) skipSpace(i int) int {
	c := 0

	for {
		idx := l.index + i + c
		if idx >= len(l.source) {
			return c
		}

		t := l.source[idx]
		if !(t == ' ' || t == '\t' || t == '-') {
			return c
		}
		c++
	}
}

func (l *controlLexer) nextClosingIndex() int {
	for i := l.index; i < len(l.source); i++ {
		if l.source[i] == '}' {
			return i - l.index
		}
	}

	return -1
}

func (l *controlLexer) next() symbol {
	s := symbol{
		start: l.index,
	}

	if l.remainingCount() == 0 {
		return symbol{t: EOF}
	}

	if l.isAt(0, "${") {
		end := l.nextClosingIndex()

		if end > -1 {
			l.index += end + 1
			s.t = Print
			s.end = l.index

			return s
		}
	}

	if l.isAt(0, "@{") {
		end := l.nextClosingIndex()

		if end > -1 {
			l.index += end + 1
			s.t = Visit
			s.end = l.index

			return s
		}
	}

	if l.isAt(0, "{") {
		end := l.nextClosingIndex()

		if end > -1 {
			s.end = l.index + end + 1
			space := l.skipSpace(1)

			if l.isAt(space+1, "end") {
				l.index += end + 1
				s.t = End

				return s
			}

			if l.isAt(space+1, "if") {
				l.index += end + 1
				s.t = If

				return s
			}

			if l.isAt(space+1, "else if") {
				l.index += end + 1
				s.t = ElseIf

				return s
			}

			if l.isAt(space+1, "else") {
				l.index += end + 1
				s.t = Else

				return s
			}

			if l.isAt(space+1, "for") {
				l.index += end + 1
				s.t = For

				return s
			}

			if l.isAt(space+1, "indent") {
				l.index += end + 1
				s.t = Indent

				return s
			}
		}
	}

	i := l.index
	for ; i < len(l.source); i++ {
		if i > l.index && (l.source[i] == '$' || l.source[i] == '{' || l.source[i] == '@') {
			break
		}
	}

	l.index = i
	s.t = Text
	s.end = i

	return s
}
