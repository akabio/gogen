package gogen

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/akabio/gogen/common"
	"gitlab.com/akabio/gogen/ggerr"
)

type opType int

const (
	unknownOp opType = iota
	intOp
	floatOp
	stringOp
	sliceOp
	boolOp
)

func evaluateBinaryOperation(left interface{}, op string, right interface{}, loc common.Location) (interface{}, error) {
	lv := reflect.ValueOf(left)
	rv := reflect.ValueOf(right)
	lt := typeOf(left)
	rt := typeOf(right)

	switch lt {
	case intOp:
		switch rt {
		case intOp:
			r, ok := intBinOp(op, lv.Int(), rv.Int())
			if ok {
				return r, nil
			}
		case floatOp:
			r, ok := floatBinOp(op, float64(lv.Int()), rv.Float())
			if ok {
				return r, nil
			}
		}
	case floatOp:
		switch rt {
		case floatOp:
			r, ok := floatBinOp(op, lv.Float(), rv.Float())
			if ok {
				return r, nil
			}
		case intOp:
			r, ok := floatBinOp(op, lv.Float(), float64(rv.Int()))
			if ok {
				return r, nil
			}
		}
	case stringOp:
		switch rt {
		case stringOp:
			switch op {
			case "+":
				return lv.String() + rv.String(), nil
			case "==":
				return lv.String() == rv.String(), nil
			case "!=":
				return lv.String() != rv.String(), nil
			}
		case intOp:
			if op == "*" {
				return strings.Repeat(lv.String(), int(rv.Int())), nil
			}
		}
		if op == "+" {
			return lv.String() + fmt.Sprint(right), nil
		}
	case sliceOp:
		if rt == sliceOp {
			if op == "+" {
				let := reflect.TypeOf(left)
				ret := reflect.TypeOf(right)
				length := lv.Len() + rv.Len()

				if let == ret {
					// if both types are the same we can create a slice of this type
					ns := reflect.MakeSlice(let, length, length)
					for i := 0; i < lv.Len(); i++ {
						ns.Index(i).Set(lv.Index(i))
					}
					for i := 0; i < rv.Len(); i++ {
						ns.Index(lv.Len() + i).Set(rv.Index(i))
					}
					return ns.Interface(), nil
				}

				ns := make([]interface{}, lv.Len()+lv.Len())
				for i := 0; i < lv.Len(); i++ {
					ns[i] = lv.Index(i).Interface()
				}
				for i := 0; i < rv.Len(); i++ {
					ns[lv.Len()+i] = rv.Index(i).Interface()
				}
				return ns, nil
			}
		}
	case boolOp:
		if rt == boolOp {
			switch op {
			case "&&":
				return lv.Bool() && rv.Bool(), nil
			case "||":
				return lv.Bool() || rv.Bool(), nil
			case "==":
				return lv.Bool() == rv.Bool(), nil
			case "!=":
				return lv.Bool() != rv.Bool(), nil
			}
		}
	}

	return nil, ggerr.NewError(loc, fmt.Errorf("can not evaluate operation %T %v %T", left, op, right))
}

func intBinOp(op string, l, r int64) (interface{}, bool) {
	switch op {
	case "<":
		return l < r, true
	case ">":
		return l > r, true
	case "<=":
		return l <= r, true
	case ">=":
		return l >= r, true
	case "==":
		return l == r, true
	case "!=":
		return l != r, true
	case "*":
		return l * r, true
	case "/":
		return l / r, true
	case "%":
		return l % r, true
	case "+":
		return l + r, true
	case "-":
		return l - r, true
	}
	return nil, false
}

func floatBinOp(op string, l, r float64) (interface{}, bool) {
	switch op {
	case "<":
		return l < r, true
	case ">":
		return l > r, true
	case "<=":
		return l <= r, true
	case ">=":
		return l >= r, true
	case "==":
		return l == r, true
	case "!=":
		return l != r, true
	case "*":
		return l * r, true
	case "/":
		return l / r, true
	case "+":
		return l + r, true
	case "-":
		return l - r, true
	}
	return nil, false
}

func typeOf(v interface{}) opType {
	switch v.(type) {
	case int, uint, int8, uint8, int16, uint16, int32, uint32, int64, uint64:
		return intOp
	case float32, float64:
		return floatOp
	case string:
		return stringOp
	case bool:
		return boolOp
	}
	value := reflect.ValueOf(v)
	switch value.Kind() {
	case reflect.Slice:
		return sliceOp
	}

	return unknownOp
}
