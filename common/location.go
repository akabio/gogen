package common

type Location struct {
	Source string
	Line   int
	Column int
}

func Loc(s string, l, c int) Location {
	return Location{s, l, c}
}

func (l *Location) Add(o Location) Location {
	return Location{l.Source, l.Line + o.Line, l.Column + o.Column}
}

type Range struct {
	From Location
	To   Location
}
