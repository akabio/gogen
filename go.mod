module gitlab.com/akabio/gogen

go 1.18

require (
	github.com/akabio/expect v0.10.0
	github.com/antlr4-go/antlr/v4 v4.13.0
	github.com/davecgh/go-spew v1.1.1
	github.com/ghodss/yaml v1.0.0
	github.com/pkg/errors v0.9.1
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/kr/pretty v0.2.1 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	golang.org/x/exp v0.0.0-20230515195305-f3d0a9c9a5cc // indirect
)
