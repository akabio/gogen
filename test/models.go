package test

type Empty struct {
}

var Models = map[string]interface{}{
	"Basic":            basic,
	"Loop":             loop,
	"If":               ifModel,
	"Method":           methods,
	"Expressions":      expressions,
	"Filters":          filters,
	"Visitors":         visitors,
	"Comments":         comments,
	"Errors":           errors,
	"Silentwhitespace": silentwhitespace,
	"Methods":          methods,
	"Literals":         literals,
	"Whitespace":       whitespace,
	"Import":           import_,
	"Variables":        variables,
	"Indent":           indent,
	"Empty":            &Empty{},
	"Selectors":        selectors,
}
