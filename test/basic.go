package test

type Basic struct {
	Title string
	Meta  Meta
	Slice []string
}

type Meta struct {
	Name  string
	Count int
}

var basic = &Basic{
	Title: "TITLE",
	Meta: Meta{
		Name:  "NAME",
		Count: 3,
	},
	Slice: []string{"X", "Y", "Z"},
}
