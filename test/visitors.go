package test

type Visitors struct {
	Meta         Meta
	NilInterface interface{}
	Slice        []Meta
	SliceP       *[]Meta
	SliceWithP   []*Meta
	SlicePWithP  *[]*Meta
}

var visitors = &Visitors{
	Meta: Meta{
		Name:  "NAME",
		Count: 3,
	},
	Slice:       []Meta{{Name: "f"}, {Name: "o"}, {Name: "o"}},
	SliceP:      &[]Meta{{Name: "f"}, {Name: "o"}, {Name: "o"}},
	SliceWithP:  []*Meta{{Name: "f"}, {Name: "o"}, {Name: "o"}},
	SlicePWithP: &[]*Meta{{Name: "f"}, {Name: "o"}, {Name: "o"}},
}
