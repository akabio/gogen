package test

import (
	"math/rand"
	"testing"

	"gitlab.com/akabio/gogen"
	"gitlab.com/akabio/gogen/internal/parser"
)

type BI struct {
	Floats []float32
}

// 10000 582239846 ns/op
//       306186353 ns/op
//         3873665 ns/op

func BenchmarkSlice(t *testing.B) {
	bi := &BI{}
	for i := 0; i < 10000; i++ {
		bi.Floats = append(bi.Floats, rand.Float32()) //nolint:gosec
	}

	ast, err := parser.Parse(`
main visitor:
test.BI type:
  {for x in .Floats}${x},{end}`, "test.gg", func(i string) (string, string, error) {
		return "", "", nil
	})
	if err != nil {
		t.Fatal(err)
	}

	t.ResetTimer()

	for i := 0; i < t.N; i++ {
		_, err := gogen.Execute(ast, bi)
		if err != nil {
			t.Fatal(err)
		}
	}
}
