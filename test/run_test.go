package test

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
	"testing"

	"github.com/akabio/expect"
	"gitlab.com/akabio/gogen"
	"gitlab.com/akabio/gogen/internal/parser"
	"gopkg.in/yaml.v2"
)

type TestSuite struct {
	Name  string  `json:"name"`
	Model string  `json:"model"`
	Tests []*Test `json:"tests"`
}

type Test struct {
	Name     string   `json:"name"`
	Only     bool     `json:"only"`
	Template string   `json:"template"`
	Expected string   `json:"expected"`
	Errors   []string `json:"errors"`
	Model    string   `json:"model"`
}

func TestAll(t *testing.T) {
	files, err := ioutil.ReadDir("./")
	if err != nil {
		t.Fatal(err)
	}
	suites := []*TestSuite{}

	for _, file := range files {
		name := file.Name()
		if strings.HasSuffix(name, ".yaml") {
			d, err := ioutil.ReadFile(name)
			if err != nil {
				t.Fatal(err)
			}
			suite := &TestSuite{}
			err = yaml.Unmarshal(d, &suite)
			if err != nil {
				t.Fatal(err)
			}
			suite.Name = name[:len(name)-5]
			suites = append(suites, suite)
		}
	}

	only := false
	for _, suite := range suites {
		for _, t := range suite.Tests {
			if t.Only {
				only = true
			}
		}
	}

	for _, suite := range suites {
		t.Run(suite.Name, func(t *testing.T) {
			suite := suite
			for _, tc := range suite.Tests {
				if !only || tc.Only {
					t.Run(tc.Name, func(t *testing.T) {
						modelName := suite.Model
						if tc.Model != "" {
							modelName = tc.Model
						}
						model := Models[modelName]

						ast, errs := parser.ParseDetailed(tc.Template, asFile(tc.Name), imports)

						// fmt.Println(tc.Name)
						// d, _ := yaml.Marshal(ast)
						// fmt.Println(string(d))

						result := ""

						if !errs.Has() {
							result, errs = gogen.ExecuteDetailed(ast, model,
								gogen.FilterOption("customFilter", customFilter),
								gogen.FilterOption("customStringErrorFilter", customStringErrorFilter),
								itemFilter,
								gogen.WithVar("global", "GLOBAL VALUE"),
							)
						}

						expect.Value(t, "result", result).ToBe(tc.Expected)
						expect.Value(t, "errors", errs.Get()).ToCount(len(tc.Errors))
						for i, erra := range errs.Get() {
							if i < len(tc.Errors) {
								expect.Value(t, "error", erra.Error()).ToBe(tc.Errors[i])
							}
							if i >= len(tc.Errors) {
								t.Error(erra)
							}
						}
					})
				}
			}
		})
	}

	if only {
		t.Errorf("Test allways fails when only activated")
	}
}

var anum = regexp.MustCompile(`[^a-zA-Z0-9]`)

func asFile(s string) string {
	return anum.ReplaceAllString(s, "") + ".gg"
}

func imports(p string) (string, string, error) {
	s, err := ioutil.ReadFile(p + ".gg")
	if err != nil {
		return "", "", err
	}
	return string(s), p + ".gg", nil
}

func customFilter(i string) string {
	return strings.Join(strings.Split(i, ""), "-")
}

func customStringErrorFilter(i string) (string, error) {
	if i == "true" {
		return "TRUE", nil
	}
	return "", fmt.Errorf("only true allowed")
}
