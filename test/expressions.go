package test

type Expressions struct {
	I10      int
	I0       int
	IntSlice []int
	StrSlice []string
}

var expressions = &Expressions{
	I10:      10,
	I0:       0,
	IntSlice: []int{4, 7},
	StrSlice: []string{"foo", "bar"},
}
