package test

type Selectors struct {
	Slice  []string
	Map    map[string]string
	Field  string
	Index  int
	String string
	Me     *Selectors
}

var selectors = func() *Selectors {
	s := &Selectors{
		Slice:  []string{"a", "b", "c", "d"},
		Map:    map[string]string{"a": "Aa", "b": "Bb", "c": "Cc", "d": "Dd"},
		Field:  "b",
		Index:  1,
		String: "abcd",
	}
	s.Me = s
	return s
}()
