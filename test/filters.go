package test

import "gitlab.com/akabio/gogen"

type Filters struct {
	String   string
	Array    [3]string
	Slice    []string
	NilSlice []string
	Items    []*FilterItem
	Map      map[string]int
	NilMap   map[string]int
}

type FilterItem struct {
	Count int
}

var filters = &Filters{
	String: "foo",
	Array:  [3]string{"f", "o", "o"},
	Slice:  []string{"f", "o", "o"},
	Items:  []*FilterItem{{1}, {2}, {3}},
	Map:    map[string]int{"a": 1, "b": 2, "c": 3},
}

var itemFilter = gogen.FilterOption("itemf", func(items []*FilterItem) []*FilterItem {
	return items[1:]
})
