package test

type Indent struct {
	Multiline string
	Number    int
}

var indent = &Indent{
	Multiline: `First μline
Second μLine
Third μLine`,
	Number: 5,
}
