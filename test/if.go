package test

type If struct {
	True  bool
	False bool

	NilString    *string
	NilStruct    *IfStruct
	NilInterface IfInterface
	Slice        []int
	EmptySlice   []int
	NilSlice     []int
	EmptyStruct  IfStruct
}

type IfInterface interface {
	Foo()
}

type IfStruct struct {
	Foo bool
}

var ifModel = &If{
	True:       true,
	Slice:      []int{1, 2, 3},
	EmptySlice: []int{},
}
