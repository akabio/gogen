package test

type Comments struct {
	A *CommentsA
	B *CommentsB
}

type CommentsA struct {
}

type CommentsB struct {
}

var comments = &Comments{A: &CommentsA{}, B: &CommentsB{}}
