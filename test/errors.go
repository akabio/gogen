package test

type Errors struct {
	Foo   ErrorsFoo
	Slice []*ErrorsStruct
	Nil   *ErrorsStruct
}

type ErrorsFoo int

type ErrorsStruct struct {
	Foo string
}

var errors = &Errors{
	Slice: []*ErrorsStruct{{Foo: "Nr1"}, {Foo: "Nr2"}},
}
