#/bin/bash

# wget https://www.antlr.org/download/antlr-4.13.0-complete.jar
cd ./parser
java -jar ../bin/antlr-4.13.0-complete.jar -no-listener -visitor -o ./internal/ggt -package ggt -Dlanguage=Go ./GoGenTemplate.g4
