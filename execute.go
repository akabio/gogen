package gogen

import (
	"fmt"
	"reflect"
	"strings"
	"unicode/utf8"

	"gitlab.com/akabio/gogen/ast"
	"gitlab.com/akabio/gogen/ggerr"
)

type Option interface {
	apply(*rt) error
}

type filterOption struct {
	name   string
	filter interface{}
}

func (f *filterOption) apply(rt *rt) error {
	return rt.filters.add(f.name, f.filter)
}

func FilterOption(name string, f interface{}) Option {
	return &filterOption{
		name:   name,
		filter: f,
	}
}

type varOption struct {
	name  string
	value interface{}
}

func (v *varOption) apply(rt *rt) error {
	rt.pushVar(v.name, v.value)

	return nil
}

func WithVar(name string, f interface{}) Option {
	return &varOption{
		name:  name,
		value: f,
	}
}

func Execute(ast *ast.AST, model interface{}, options ...Option) (string, error) {
	r, err := ExecuteDetailed(ast, model, options...)
	if err.Has() {
		return "", err
	}

	return r, nil
}

func ExecuteDetailed(a *ast.AST, model interface{}, options ...Option) (string, *ggerr.MultiError) {
	errs := ggerr.NewMultiError()
	output := &output{}
	rt := &rt{
		ast:            a,
		visitor:        []string{"main"},
		errors:         errs,
		output:         output,
		filters:        newFilters(),
		importRuntimes: map[*ast.Import]*rt{},
	}

	opts := []Option{}
	opts = append(opts, defaultFilters...)
	opts = append(opts, options...)

	for _, o := range opts {
		err := o.apply(rt)
		if err != nil {
			errs.Push(err)

			return "", errs
		}
	}

	// evaluate import params

	err := rt.evalImports()
	if err != nil {
		errs.PushLocation(a.Location, err)
	}

	err = rt.render(model)
	if err != nil {
		errs.PushLocation(a.Location, err)
	}

	return output.output.String(), errs
}

type rt struct {
	ast            *ast.AST
	visitor        []string
	variables      []variable
	model          []interface{}
	importRuntimes map[*ast.Import]*rt
	indent         []int
	output         *output
	errors         *ggerr.MultiError
	filters        *filters
}

type output struct {
	output    strings.Builder
	curIndent int
}

type variable struct {
	name  string
	value interface{}
}

func (r *rt) pushVisitor(visitor string) {
	r.visitor = append(r.visitor, visitor)
}

func (r *rt) popVisitor() {
	r.visitor = r.visitor[:len(r.visitor)-1]
}

func (r *rt) getVisitor() string {
	return r.visitor[len(r.visitor)-1]
}

func (r *rt) pushModel(model interface{}) {
	r.model = append(r.model, model)
}

func (r *rt) popModel() {
	r.model = r.model[:len(r.model)-1]
}

func (r *rt) getModel() interface{} {
	return r.model[len(r.model)-1]
}

func (r *rt) pushVar(name string, value interface{}) {
	r.variables = append(r.variables, variable{
		name:  name,
		value: value,
	})
}

func (r *rt) popVar() {
	r.variables = r.variables[:len(r.variables)-1]
}

func (r *rt) pushCurrentIndent() {
	r.pushIndent(r.output.curIndent)
}

func (r *rt) pushIndent(c int) {
	r.indent = append(r.indent, c)
}

func (r *rt) popIndent() {
	r.indent = r.indent[:len(r.indent)-1]
}

func (r *rt) getIndent() int {
	if len(r.indent) > 0 {
		return r.indent[len(r.indent)-1]
	}
	return 0
}

func (o *output) append(indent int, s string) {
	lines := strings.Split(s, "\n")

	for i, line := range lines {
		toIndent := indent - o.curIndent
		if toIndent > 0 {
			o.output.WriteString(strings.Repeat(" ", toIndent))
			o.curIndent += toIndent
		}

		o.output.WriteString(line)
		o.curIndent += utf8.RuneCountInString(line)

		if i < len(lines)-1 {
			o.output.WriteString("\n")
			o.curIndent = 0
		}
	}
}

func (r *rt) append(s string) {
	r.output.append(r.getIndent(), s)
}

// getVar finds the variable by name, searching from top of stack
// so deeper scopes can overwrite upper scopes vars (might be
// better to not allow overriding in the first place?)
func (r *rt) getVar(name string) (interface{}, error) {
	for i := len(r.variables) - 1; i >= 0; i-- {
		if r.variables[i].name == name {
			return r.variables[i].value, nil
		}
	}
	return nil, fmt.Errorf("variable '%v' does not exist", name)
}

func (r *rt) evalImports() error {
	for _, imp := range r.ast.Imports {
		rt := &rt{
			ast:            imp.AST,
			variables:      []variable{},
			importRuntimes: map[*ast.Import]*rt{},
		}
		r.importRuntimes[imp] = rt

		for _, p := range imp.Parameters {
			val, err := r.evalExpression(p.Value, nil)
			if err != nil {
				return err
			}

			rt.variables = append(rt.variables, variable{
				name:  p.Name,
				value: val,
			})
		}
		err := rt.evalImports()
		if err != nil {
			return err
		}
	}
	return nil
}

func (r *rt) evalSelector(sel ast.Selector, model interface{}) (interface{}, error) {
	switch selector := sel.(type) {
	case *ast.FieldSelector:
		m := reflect.ValueOf(model)
		if m.Kind() == reflect.Ptr && m.IsNil() {
			return nil, ggerr.NewError(sel.Start(), fmt.Errorf("can not access field '%v' on nil value", selector.Name))
		}
		for m.Kind() == reflect.Ptr {
			m = m.Elem()
		}

		kind := m.Kind()

		// check if it's a struct field
		if kind == reflect.Struct {
			f := m.FieldByName(selector.Name)
			if f.IsValid() {
				return f.Interface(), nil
			}
		}

		// check if it's a method
		f := m.MethodByName(selector.Name)
		if !f.IsValid() {
			// if it's a method with pointer receiver
			if m.CanAddr() {
				f = m.Addr().MethodByName(selector.Name)
			}
		}
		if f.IsValid() {
			ret := f.Call([]reflect.Value{})
			if len(ret) != 1 {
				return nil, fmt.Errorf("can only call funcs with 1 return argument %T", model)
			}
			return ret[0].Interface(), nil
		}

		return nil, ggerr.NewError(selector.Start(), fmt.Errorf("invalid field or method '%v' on type '%T'", selector.Name, m.Interface()))

	case *ast.VariableSelector:
		val, err := r.getVar(selector.Name)
		if err != nil {
			return nil, ggerr.NewError(selector.Start(), err)
		}
		return val, nil

	case *ast.IndexSelector:
		val, err := r.evalExpression(selector.Expression, r.getModel())
		if err != nil {
			return nil, err
		}

		m := reflect.ValueOf(model)
		if m.Kind() == reflect.Ptr && m.IsNil() {
			return nil, ggerr.NewError(sel.Start(), fmt.Errorf("can not access element %v on nil value", val))
		}
		for m.Kind() == reflect.Ptr {
			m = m.Elem()
		}

		kind := m.Kind()

		// check if it's an slice or Array
		switch kind {
		case reflect.Array, reflect.Slice:
			idx, err := asInt(val)
			if err != nil {
				return nil, ggerr.NewError(sel.Start(), fmt.Errorf("can not index slice/array, %w", err))
			}

			f := m.Index(idx)
			return f.Interface(), nil

		case reflect.Map:
			f := m.MapIndex(reflect.ValueOf(val))
			zv := reflect.Value{}
			if f == zv {
				return nil, nil
			}
			return f.Interface(), nil

		default:
			return nil, ggerr.NewError(sel.Start(), fmt.Errorf("can not index type '%v'", kind))
		}

	case *ast.SliceSelector:
		var err error
		var from any
		var to any

		if selector.From != nil {
			from, err = r.evalExpression(selector.From, r.getModel())
			if err != nil {
				return nil, err
			}
		}

		if selector.To != nil {
			to, err = r.evalExpression(selector.To, r.getModel())
			if err != nil {
				return nil, err
			}
		}

		m := reflect.ValueOf(model)
		if m.Kind() == reflect.Ptr && m.IsNil() {
			return nil, ggerr.NewError(sel.Start(), fmt.Errorf("can not slice nil value"))
		}
		for m.Kind() == reflect.Ptr {
			m = m.Elem()
		}

		kind := m.Kind()

		// check if it's an slice or Array
		switch kind {
		case reflect.Array, reflect.Slice:
			idxFrom := 0
			if from != nil {
				idxFrom, err = asInt(from)
				if err != nil {
					return nil, ggerr.NewError(sel.Start(), fmt.Errorf("can not index slice/array, %w", err))
				}
			}

			idxTo := m.Len()
			if to != nil {
				idxTo, err = asInt(to)
				if err != nil {
					return nil, ggerr.NewError(sel.Start(), fmt.Errorf("can not index slice/array, %w", err))
				}
			}

			f := m.Slice(idxFrom, idxTo)
			return f.Interface(), nil

		default:
			return nil, ggerr.NewError(sel.Start(), fmt.Errorf("can not index type '%v'", kind))
		}

	case *ast.CallSelector:
		f := reflect.ValueOf(model)
		if f.Kind() != reflect.Func {
			return nil, fmt.Errorf("try to call a non function %T", model)
		}
		ret := f.Call([]reflect.Value{})
		if len(ret) != 1 {
			return nil, fmt.Errorf("can only call funcs with 1 return argument %T", model)
		}
		return ret[0].Interface(), nil

	case *ast.SelectorChain:
		return r.evalSelectorChain(selector.Selectors, model)
	}
	panic(fmt.Errorf("unknown selector %T", sel))
}

func (r *rt) evalSelectorChain(sels []ast.Selector, model interface{}) (interface{}, error) {
	if len(sels) == 0 {
		return model, nil
	}

	sel := sels[0]
	val, err := r.evalSelector(sel, model)
	if err != nil {
		return nil, err
	}

	return r.evalSelectorChain(sels[1:], val)
}

func (r *rt) evalExpression(ex ast.Expression, model interface{}) (interface{}, error) {
	switch x := ex.(type) {
	case *ast.SelectorChain:
		return r.evalSelectorChain(x.Selectors, model)
	case *ast.BoolLiteral:
		return x.Value, nil
	case *ast.IntLiteral:
		return x.Value, nil
	case *ast.FloatLiteral:
		return x.Value, nil
	case *ast.StringLiteral:
		return x.Value, nil
	case *ast.Filter:
		left, err := r.evalExpression(x.Expression, model)
		if err != nil {
			return nil, err
		}

		// builtins
		if x.Name == "type" {
			return fmt.Sprintf("%T", left), nil
		}

		if x.Name == "len" {
			val := reflect.ValueOf(left)
			switch val.Kind() {
			case reflect.Slice, reflect.Array, reflect.Chan, reflect.Map, reflect.String:
				return val.Len(), nil
			}
		}

		ret, err, found := r.filters.apply(x.Name, left)
		if found {
			if err != nil {
				return ret, ggerr.NewError(ex.Start(), err)
			}
			return ret, err
		}

		return nil, ggerr.NewError(ex.Start(), fmt.Errorf("can not apply filter '%v' on type %T", x.Name, left))

	case *ast.BinaryOperation:
		left, err := r.evalExpression(x.Left, model)
		if err != nil {
			return nil, err
		}
		right, err := r.evalExpression(x.Right, model)
		if err != nil {
			return nil, err
		}
		return evaluateBinaryOperation(left, x.Operator, right, x.Location)
	case *ast.NotOperation:
		val, err := r.evalExpression(x.Expression, model)
		if err != nil {
			return nil, err
		}
		b, isbool := val.(bool)
		if isbool {
			return !b, nil
		}
		// TODO should be an error?
		return false, nil

	case *ast.VarExists:
		for _, v := range r.variables {
			if v.name == x.Name {
				return true, nil
			}
		}
		return false, nil
	}

	panic(fmt.Errorf("unknown expression type %T", ex))
}

func (r *rt) renderEcho(n *ast.EchoNode, model interface{}) {
	val, err := r.evalExpression(n.Expression, model)
	if err != nil {
		r.errors.Push(err)
	}
	r.append(fmt.Sprint(val))
}

func (r *rt) renderVisit(n *ast.VisitNode, model interface{}) {
	val, err := r.evalExpression(n.Expression, model)
	if err != nil {
		r.errors.Push(err)
		return
	}

	// evaluate param expressions
	paramValues := make([]interface{}, len(n.Parameters))
	for i, param := range n.Parameters {
		val, err := r.evalExpression(param.Value, model)
		if err != nil {
			r.errors.Push(err)
		}
		paramValues[i] = val
	}

	if n.Package != "" {
		r.renderImportVisit(n, model, val, paramValues)
		return
	}

	r.renderLocalVisit(n, model, val, paramValues)
}

func (r *rt) renderImportVisit(n *ast.VisitNode, model interface{}, val interface{}, paramValues []interface{}) {
	imp := findPackage(r.ast, n.Package)
	if imp == nil {
		r.errors.Push(ggerr.NewError(n.Start(), fmt.Errorf("package %v not found", n.Package)))
		return
	}

	sub := r.importRuntimes[imp]
	sub.visitor = []string{n.Visitor}
	sub.errors = r.errors
	sub.output = r.output
	sub.filters = r.filters
	sub.indent = r.indent

	sub.renderLocalVisit(n, model, val, paramValues)
}

func (r *rt) renderLocalVisit(n *ast.VisitNode, _ interface{}, val interface{}, paramValues []interface{}) {
	if n.Visitor != "" {
		r.pushVisitor(n.Visitor)
		defer r.popVisitor()
	}

	tpl, err := r.getTemplate(val)
	if err != nil {
		r.errors.Push(ggerr.NewError(n.Start(), err))
		return
	}

	// set default parameters
	for _, param := range tpl.DefaultParameters {
		// expression can only be a literal so there is no model needed
		val, err := r.evalExpression(param.Value, nil)
		if err != nil {
			r.errors.Push(err)
		}
		r.pushVar(param.Name, val)
		defer r.popVar()
	}

	// overwrite with actual parameters when set
	for i, param := range n.Parameters {
		r.pushVar(param.Name, paramValues[i])
		defer r.popVar()
	}

	err = r.render(val)
	if err != nil {
		r.errors.Push(ggerr.NewError(n.Start(), err))
	}
}

func (r *rt) renderIndent(n *ast.IndentNode, model interface{}) {
	if n.Expression == nil {
		r.pushCurrentIndent()
		defer r.popIndent()

	} else {
		val, err := r.evalExpression(n.Expression, model)
		if err != nil {
			r.errors.Push(err)
		}
		valInt, isInt := val.(int64)
		if !isInt {
			r.errors.Push(fmt.Errorf("indent expression must return an int but it returned %T", val))
		}

		r.pushIndent(int(valInt))
		defer r.popIndent()
	}

	for _, n := range n.Nodes {
		r.renderNode(n, model)
	}
}

func (r *rt) renderIf(n *ast.IfNode, model interface{}) {
	val, err := r.evalExpression(n.Expression, model)
	if err != nil {
		r.errors.Push(err)
		return
	}

	var nodes []ast.Node
	if evalsTrue(val) {
		nodes = n.IfNodes
	} else {
		nodes = n.ElseNodes
	}

	for _, n := range nodes {
		r.renderNode(n, model)
	}
}

func (r *rt) renderLoop(l *ast.ForNode, model interface{}) {
	val, err := r.evalExpression(l.Expression, model)
	if err != nil {
		r.errors.Push(err)
		return
	}

	its, err := iteratorFor(val)
	if err != nil {
		r.errors.Push(err)
		return
	}

	for i, e := range its {

		r.pushVar(l.Value, e.value)
		if l.Key != "" {
			r.pushVar(l.Key, e.key)
		}

		if l.First {
			r.pushVar("first", i == 0)
		}
		if l.Last {
			r.pushVar("last", i == len(its)-1)
		}
		if l.Even {
			r.pushVar("even", i%2 == 0)
		}

		for _, n := range l.Nodes {
			r.renderNode(n, model)
		}

		r.popVar()
		if l.Key != "" {
			r.popVar()
		}
		if l.First {
			r.popVar()
		}
		if l.Last {
			r.popVar()
		}
		if l.Even {
			r.popVar()
		}
	}
}

func (r *rt) renderNode(n ast.Node, model interface{}) {
	switch node := n.(type) {
	case *ast.TextNode:
		r.append(node.Text)
	case *ast.EchoNode:
		r.renderEcho(node, model)
	case *ast.VisitNode:
		r.renderVisit(node, model)
	case *ast.ForNode:
		r.renderLoop(node, model)
	case *ast.IfNode:
		r.renderIf(node, model)
	case *ast.IndentNode:
		r.renderIndent(node, model)
	default:
		panic(fmt.Errorf("unknown node %T", n))
	}
}

func (r *rt) render(model interface{}) error {
	tpl, err := r.getTemplate(model)
	if err != nil {
		return err
	}

	r.pushModel(model)
	for _, n := range tpl.Nodes {
		r.renderNode(n, model)
	}
	r.popModel()
	return nil
}

func (r *rt) getTemplate(model interface{}) (*ast.Template, error) {
	visitor, err := r.getVisitorInstance()
	if err != nil {
		return nil, err
	}

	tp := fmt.Sprintf("%T", model)
	// normalize type by striping pointers
	tp = strings.ReplaceAll(tp, "*", "")

	for _, t := range visitor.Templates {
		if t.Name == tp {
			return t, nil
		}
	}

	// no exact type found, check for default
	for _, t := range visitor.Templates {
		if t.Name == "*" {
			return t, nil
		}
	}

	// no default found, error
	return nil, fmt.Errorf("could not find template for type '%v' for visitor '%v'", tp, r.getVisitor())
}

func (r *rt) getVisitorInstance() (*ast.Visitor, error) {
	for _, v := range r.ast.Visitors {
		if v.Name == r.getVisitor() {
			return v, nil
		}
	}
	return nil, fmt.Errorf("no visitor '%v' defined", r.getVisitor())
}

func findPackage(a *ast.AST, pkg string) *ast.Import {
	for _, imp := range a.Imports {
		if imp.Name == pkg {
			return imp
		}
	}
	return nil
}
