package ggerr

import (
	"fmt"
	"strings"

	"gitlab.com/akabio/gogen/common"
)

type MultiError struct {
	errs []error
}

type Error struct {
	err      error
	location common.Location
}

func (e *Error) Error() string {
	return fmt.Sprintf("%v line %v col %v: %v", e.location.Source, e.location.Line+1, e.location.Column, e.err.Error())
}

func (e *Error) Location() (string, int, int) {
	return e.location.Source, e.location.Line, e.location.Column
}

func NewMultiError() *MultiError {
	return &MultiError{}
}

func (e *MultiError) Error() string {
	messages := []string{}
	for _, e := range e.errs {
		messages = append(messages, e.Error())
	}
	return strings.Join(messages, "\n")
}

func NewError(l common.Location, err error) error {
	return &Error{
		err:      err,
		location: l,
	}
}

func (e *MultiError) Push(err error) {
	if err != nil {
		e.errs = append(e.errs, err)
	}
}

func (e *MultiError) PushLocation(l common.Location, err error) {
	if err != nil {
		e.errs = append(e.errs, &Error{location: l, err: err})
	}
}

func (e *MultiError) PushAll(errs *MultiError) {
	e.errs = append(e.errs, errs.errs...)
}

func (e *MultiError) Get() []error {
	return e.errs
}

func (e *MultiError) Has() bool {
	return len(e.errs) > 0
}
