package gogen

import (
	"gitlab.com/akabio/gogen/ast"
	"gitlab.com/akabio/gogen/ggerr"
	"gitlab.com/akabio/gogen/internal/parser"
)

// Parse a gogen template
func Parse(code, source string, imports func(i string) (string, string, error)) (*ast.AST, error) {
	return parser.Parse(code, source, imports)
}

// ParseDetailed will always return an error object which contains details about all found errors
func ParseDetailed(code, source string, imports func(i string) (string, string, error)) (*ast.AST, *ggerr.MultiError) {
	return parser.ParseDetailed(code, source, imports)
}
