package main

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/davecgh/go-spew/spew"
	"github.com/ghodss/yaml"
	"gitlab.com/akabio/gogen"
	"gitlab.com/akabio/gogen/internal/parser"
	"gitlab.com/akabio/gogen/test"
)

func init() {
	spew.Config.DisableCapacities = true
	spew.Config.DisablePointerAddresses = true
	spew.Config.SortKeys = true
	spew.Config.Indent = "  "
}

func main() {
	r, err := readAll()
	if err != nil {
		panic(err)
	}

	mdc, err := os.ReadFile("cmd/doc/markdown.gg")
	if err != nil {
		log.Fatal(err)
	}

	ast, err := parser.Parse(string(mdc), "markdown.gg", func(i string) (string, string, error) { return "", "", nil })
	if err != nil {
		log.Fatal("parse", err)
	}

	res, err := gogen.Execute(ast, r)
	if err != nil {
		log.Fatal("exec", err)
	}

	err = ioutil.WriteFile("doc/doc.md", []byte(res), 0o600)
	if err != nil {
		log.Fatal("write", err)
	}

	html, err := os.ReadFile("cmd/doc/html.gg")
	if err != nil {
		log.Fatal(err)
	}

	ast, err = parser.Parse(string(html), "html.gg", func(i string) (string, string, error) { return "", "", nil })
	if err != nil {
		log.Fatal("parse", err)
	}

	res, err = gogen.Execute(ast, r)
	if err != nil {
		log.Fatal("exec", err)
	}

	err = os.WriteFile("doc/doc.html", []byte(res), 0o600)
	if err != nil {
		log.Fatal("write", err)
	}
}

func readAll() (*root, error) {
	fis, err := os.ReadDir("test")
	if err != nil {
		return nil, err
	}

	r := &root{}

	for _, f := range fis {
		if strings.HasSuffix(f.Name(), ".yaml") {
			bin, err := os.ReadFile(filepath.Join("test", f.Name()))
			if err != nil {
				return nil, err
			}
			chap := &chapter{}
			err = yaml.Unmarshal(bin, chap)
			if err != nil {
				return nil, err
			}
			if chap.Name != "" {
				r.Chapters = append(r.Chapters, chap)
				retained := []*testCase{}
				for _, tc := range chap.Tests {
					if tc.Description != "" {
						retained = append(retained, tc)

						mod := chap.Model
						if tc.Model != "" {
							mod = tc.Model
						}
						if mod != "Empty" {
							modInstance := test.Models[mod]
							tc.SerializedModel = spew.Sdump(modInstance)
						}
					}
				}
				chap.Tests = retained
			}
		}
	}

	sort.Slice(r.Chapters, func(i, j int) bool {
		return r.Chapters[i].Order < r.Chapters[j].Order
	})

	return r, nil
}
