package main

type root struct {
	Chapters []*chapter
}

type chapter struct {
	Name  string      `json:"name"`
	Order int         `json:"order"`
	Model string      `json:"model"`
	Tests []*testCase `json:"tests"`
}

type testCase struct {
	Name            string   `json:"name"`
	Description     string   `json:"description"`
	Template        string   `json:"template"`
	Expected        string   `json:"expected"`
	Errors          []string `json:"errors"`
	Model           string   `json:"model"`
	SerializedModel string
}
