package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/pkg/errors"
	"gitlab.com/akabio/gogen/ast"
	"gitlab.com/akabio/gogen/ggerr"
	"gitlab.com/akabio/gogen/internal/parser"
)

type result struct {
	Errors []errMsg `json:"errors"`
	Ast    *ast.AST `json:"ast"`
}

type errMsg struct {
	Message string `json:"message"`
	Line    int    `json:"line"`
	Column  int    `json:"column"`
}

func main() {
	dat, err := io.ReadAll(os.Stdin)
	if err != nil {
		log.Fatal(err)
	}
	ast, errs := parser.ParseDetailed(string(dat), "", func(i string) (string, string, error) {
		return "", "", nil
	})

	r := result{
		Ast: ast,
	}
	for _, e := range errs.Get() {
		ge := &ggerr.Error{}

		if errors.As(e, ge) {
			_, l, c := ge.Location()
			r.Errors = append(r.Errors, errMsg{
				Message: ge.Error(),
				Line:    l,
				Column:  c,
			})
		} else {
			r.Errors = append(r.Errors, errMsg{
				Message: e.Error(),
				Line:    0,
				Column:  0,
			})
		}
	}

	d, err := json.MarshalIndent(&r, "", "  ")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(d))
}
