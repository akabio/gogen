package gogen

import (
	"fmt"
	"reflect"

	"gitlab.com/akabio/gogen/internal/fmtsort"
)

type iterateElement struct {
	key   interface{}
	value interface{}
}

func iteratorFor(val interface{}) ([]iterateElement, error) {
	if val == nil {
		return nil, nil
	}
	ref := reflect.ValueOf(val)

	// if it's a pointer or interface, dereference it
	for ref.Kind() == reflect.Ptr || ref.Kind() == reflect.Interface {
		ref = ref.Elem()
	}

	if !hasLen(ref) {
		return iteratorForRange(val)
	}

	len := ref.Len()
	elements := make([]iterateElement, 0, len)
	switch ref.Kind() {
	case reflect.Array, reflect.Slice, reflect.Chan:
		for i := 0; i < len; i++ {
			elements = append(elements, iterateElement{
				key:   i,
				value: ref.Index(i).Interface(),
			})
		}
	case reflect.String:
		for i, char := range ref.String() {
			elements = append(elements, iterateElement{
				key:   i,
				value: string(char),
			})
		}
	case reflect.Map:
		sorted := fmtsort.Sort(ref)
		for i, k := range sorted.Key {
			elements = append(elements, iterateElement{
				key:   k.Interface(),
				value: sorted.Value[i].Interface(),
			})
		}
	}

	return elements, nil
}

func iteratorForRange(val interface{}) ([]iterateElement, error) {
	v, err := asInt(val)
	if err != nil {
		return nil, err
	}

	ies := []iterateElement{}

	for i := 0; i < v; i++ {
		ies = append(ies, iterateElement{key: i, value: i})
	}

	return ies, nil
}

func asInt(value interface{}) (int, error) {
	switch t := value.(type) {
	case int:
		return t, nil
	case int64:
		return int(t), nil
	}
	return 0, fmt.Errorf("not a number, %T", value)
}

func hasLen(v reflect.Value) bool {
	switch v.Kind() {
	case reflect.Array, reflect.Slice, reflect.Map, reflect.String, reflect.Chan:
		return true
	default:
		return false
	}
}
