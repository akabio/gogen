package ast

import (
	"strconv"

	"gitlab.com/akabio/gogen/common"
)

type AST struct {
	*BaseNode
	Visitors []*Visitor
	Imports  []*Import
}

type Import struct {
	Name       string
	AST        *AST
	Parameters []*Parameter
}

type Visitor struct {
	Name      string
	Templates []*Template
}

type Template struct {
	Name              string
	Nodes             []Node
	DefaultParameters []*Parameter
}

type Node interface {
	Start() common.Location
}

type BaseNode struct {
	Location common.Location
}

func (bn *BaseNode) Start() common.Location {
	return bn.Location
}

type TextNode struct {
	*BaseNode
	Text string
}

type EchoNode struct {
	*BaseNode
	Silent
	Expression Expression
}

type VisitNode struct {
	*BaseNode
	Visitor string
	Package string
	Silent
	Expression Expression
	Parameters []*Parameter
}

type Parameter struct {
	*BaseNode
	Name  string
	Value Expression
}

// Silent will tell if whitespace before and after a node should be removed
type Silent struct {
	Before bool
	After  bool
}

type ForNode struct {
	*BaseNode
	Key        string
	Value      string
	Expression Expression
	Nodes      []Node
	First      bool
	Last       bool
	Even       bool

	SilentFor Silent
	SilentEnd Silent
}

type IndentNode struct {
	*BaseNode
	Expression Expression
	Nodes      []Node

	SilentIndent Silent
	SilentEnd    Silent
}

type IfNode struct {
	*BaseNode
	Expression Expression
	IfNodes    []Node
	ElseNodes  []Node

	SilentIf   Silent
	SilentElse Silent
	SilentEnd  Silent
}

type Expression interface {
	Node
	String() string
}

type SelectorChain struct {
	*BaseNode
	Selectors []Selector
}

func (cs *SelectorChain) String() string {
	r := ""
	for _, sel := range cs.Selectors {
		r += sel.String()
	}
	return r
}

type BoolLiteral struct {
	*BaseNode
	Value bool
}

func (bl *BoolLiteral) String() string {
	return strconv.FormatBool(bl.Value)
}

type IntLiteral struct {
	*BaseNode
	Value int64
}

func (il *IntLiteral) String() string {
	return strconv.FormatInt(il.Value, 10)
}

type FloatLiteral struct {
	*BaseNode
	Value float64
}

func (fl *FloatLiteral) String() string {
	return strconv.FormatFloat(fl.Value, 'f', 10, 64)
}

type StringLiteral struct {
	*BaseNode
	Value string
}

func (sl *StringLiteral) String() string {
	return `"` + sl.Value + `"`
}

type NotOperation struct {
	*BaseNode
	Expression Expression
}

func (no *NotOperation) String() string {
	return `!(` + no.Expression.String() + `)`
}

type VarExists struct {
	*BaseNode
	Name string
}

func (ve *VarExists) String() string {
	return `?(` + ve.Name + `)`
}

type BinaryOperation struct {
	*BaseNode
	Left     Expression
	Right    Expression
	Operator string
}

func (bo *BinaryOperation) String() string {
	return `(` + bo.Left.String() + " " + bo.Operator + " " + bo.Right.String() + `)`
}

type Selector interface {
	Expression
}

type FieldSelector struct {
	*BaseNode
	Name string
}

func (fs *FieldSelector) String() string {
	return "." + fs.Name
}

type CallSelector struct {
	*BaseNode
}

func (cs *CallSelector) String() string {
	return "()"
}

type VariableSelector struct {
	*BaseNode
	Name string
}

func (vs *VariableSelector) String() string {
	return vs.Name
}

type IndexSelector struct {
	*BaseNode
	Expression Expression
}

func (i *IndexSelector) String() string {
	return "[" + i.Expression.String() + "]"
}

type SliceSelector struct {
	*BaseNode
	From Expression
	To   Expression
}

func (s *SliceSelector) String() string {
	from := ""
	to := ""
	if s.From != nil {
		from = s.From.String()
	}
	if s.To != nil {
		to = s.To.String()
	}
	return "[" + from + ":" + to + "]"
}

type Filter struct {
	*BaseNode
	Name       string
	Expression Expression
}

func (f *Filter) String() string {
	return "(" + f.Expression.String() + ") | " + f.Name
}

type NoExpression struct {
	*BaseNode
}

func (n *NoExpression) String() string {
	return "()"
}
