package gogen

import (
	"fmt"
	"reflect"
	"strings"
)

var defaultFilters = []Option{
	FilterOption("upper", strings.ToUpper),
	FilterOption("lower", strings.ToLower),
}

type filters struct {
	// string => (string, error?) filters are the most common kind so we do the
	// reflection in advance so we don't have to do that every time during runtime.
	strToStr    map[string]func(string) string
	strToStrErr map[string]func(string) (string, error)
	// all other types we handle by signature name(type) and do the reflection during runtime
	custom map[string]interface{}
}

func newFilters() *filters {
	return &filters{
		strToStr:    map[string]func(string) string{},
		strToStrErr: map[string]func(string) (string, error){},
		custom:      map[string]interface{}{},
	}
}

func (f *filters) apply(filter string, left interface{}) (interface{}, error, bool) {
	switch t := left.(type) {
	case string:
		ff, has := f.strToStr[filter]
		if has {
			return ff(t), nil, true
		}
		ffe, has := f.strToStrErr[filter]
		if has {
			new, err := ffe(t)
			if err != nil {
				return new, fmt.Errorf("error in filter %v, %w", filter, err), true
			}
			return new, err, true
		}
	}

	sig := fmt.Sprintf("%v(%T)", filter, left)
	cf, has := f.custom[sig]
	if has {
		fnc := reflect.ValueOf(cf)
		res := fnc.Call([]reflect.Value{reflect.ValueOf(left)})
		if len(res) == 0 {
			panic("filter needs at least one return parameter")
		}
		return res[0].Interface(), nil, true
	}

	return nil, nil, false
}

func (f *filters) add(name string, fn interface{}) error {
	fnt := reflect.TypeOf(fn)

	if fnt.Kind() != reflect.Func {
		return fmt.Errorf("filter '%v' must be a func but is of type %T", name, fn)
	}

	nIn := fnt.NumIn()

	pin := []string{}
	for i := 0; i < nIn; i++ {
		pin = append(pin, fnt.In(i).String())
	}

	nOut := fnt.NumOut()
	pout := []string{}
	for i := 0; i < nOut; i++ {
		pout = append(pout, fnt.Out(i).String())
	}

	sig := strings.Join(pin, ",") + "->" + strings.Join(pout, ",")

	switch sig {
	case "string->string":
		f.strToStr[name] = fn.(func(string) string)
	case "string->string,error":
		f.strToStrErr[name] = fn.(func(string) (string, error))
	default:
		f.custom[name+"("+strings.Join(pin, ",")+")"] = fn
	}

	return nil
}
